Title: Firefox: Moving from the Debian package to the Flatpak app (long-term?)
Category: dev
Tags: debian, firefox, flathub, flatpak, gnome, planet-debian



First, thanks to Samuel Henrique for giving notice of [recent Firefox
CVEs](https://lists.debian.org/debian-devel/2024/03/msg00299.html) in Debian
testing/unstable.

At the time I didn't want to upgrade my system (Debian Sid) due to the ongoing
[t64 transition](https://wiki.debian.org/ReleaseGoals/64bit-time) transition,
so I decided I could install the Firefox Flatpak app instead, and why not stick
to it long-term?

This blog post details all the steps, if ever others want to go the same road.


## Flatpak Installation

Disclaimer: this section is hardly anything more than a copy/paste of the
[official documentation](https://flathub.org/setup/Debian), and with time it
will get outdated, so you'd better follow the official doc.

First thing first, let's install [Flatpak](https://flatpak.org/):

```
$ sudo apt update
$ sudo apt install flatpak
```

Then the next step is to add the [Flathub](https://flathub.org/) remote
repository, from where we'll get our Flatpak applications:

```
$ flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

And that's all there is to it! Now come the optional steps.

For GNOME and KDE users, you might want to install a plugin for the software
manager specific to your desktop, so that it can support and manage Flatpak
apps:

```
$ which gnome-software  && sudo apt install gnome-software-plugin-flatpak
$ which plasma-discover && sudo apt install plasma-discover-backend-flatpak
```

And here's an additional check you can do, as it's something that did bite me
in the past: missing `xdg-portal-*` packages, that are required for Flatpak
applications to communicate with the desktop environment.  Just to be sure, you
can check the output of `apt search '^xdg-desktop-portal'` to see what's
available, and compare with the output of `dpkg -l | grep xdg-desktop-portal`.

As you can see, if you're a GNOME or KDE user, there's a portal backend for
you, and it should be installed. For reference, this is what I have on my GNOME
desktop at the moment:

```
$ dpkg -l | grep xdg-desktop-portal | awk '{print $2}'
xdg-desktop-portal
xdg-desktop-portal-gnome
xdg-desktop-portal-gtk
```


## Install the Firefox Flatpak app

This is trivial, but still, there's a question I've always asked myself: should
I install applications system-wide (aka. `flatpak --system`, the default) or
per-user (aka. `flatpak --user`)? Turns out, this questions is answered in the
[Flatpak documentation](https://docs.flatpak.org/en/latest/using-flatpak.html):

> Flatpak commands are run system-wide by default. If you are installing
> applications for day-to-day usage, it is recommended to stick with this
> default behavior.

Armed with this new knowledge, let's install the
[Firefox app](https://flathub.org/apps/org.mozilla.firefox):

```
$ flatpak install flathub org.mozilla.firefox
```

And that's about it! We can give it a go already:

```
$ flatpak run org.mozilla.firefox
```


## Data migration

At this point, running Firefox via Flatpak gives me an "empty" Firefox. That's
not what I want, instead I want my usual Firefox, with a gazillion of tabs
already opened, a few extensions, bookmarks and so on.

As it turns out, Mozilla provides a brief [doc for data
migration](https://support.mozilla.org/en-US/kb/install-firefox-linux#w_data-migration),
and it's as simple as moving the Firefox data directory around!

To clarify, we'll be copying data:

- from `~/.mozilla/` -- where the Firefox Debian package stores its data
- into `~/.var/app/org.mozilla.firefox/.mozilla/` -- where the Firefox Flatpak
  app stores its data

Let's make sure that all Firefox instances are closed, then proceed:

```
# BEWARE! Below I'm erasing data!
$ rm -fr ~/.var/app/org.mozilla.firefox/.mozilla/firefox/
$ cp -a ~/.mozilla/firefox/ ~/.var/app/org.mozilla.firefox/.mozilla/
```

To avoid confusing myself, it's also a good idea to rename the old data
directory:

```
$ mv ~/.mozilla/firefox ~/.mozilla/firefox.old.$(date --iso-8601=date)
```

At this point, `flatpak run org.mozilla.firefox` takes me to my "usual"
everyday Firefox, with all its tabs opened, pinned, bookmarked, etc.


## More integration?

After following all the steps above, I must say that I'm 99% happy. So far,
everything works as before, I didn't hit any issue, and I don't even notice
that Firefox is running via Flatpak, it's completely transparent.

So where's the 1% of unhappiness? The « Run a Command » dialog from GNOME, the
one that shows up via the keyboard shortcut `<Alt+F2>`. This is how I start my
GUI applications, and I usually run two Firefox instances in parallel (one for
work, one for personal), using the `firefox -p <profile>` command.

Given that I ran `apt purge firefox` before (to avoid confusing myself with two
installations of Firefox), now the right (and only) way to start Firefox from a
command-line is to type `flatpak run org.mozilla.firefox -p <profile>`.  Typing
that every time is way too cumbersome, so I need something quicker.

Seems like the most straightforward is to create a wrapper script:

```
$ cat /usr/local/bin/firefox 
#!/bin/sh
exec flatpak run org.mozilla.firefox "$@"
```

And now I can just hit `<Alt+F2>` and type `firefox -p <profile>` to start
Firefox with the profile I want, just as before. Neat!


## Looking forward: system updates

I usually update my system manually every now and then, via the well-known pair
of commands:

```
$ sudo apt update
$ sudo apt full-upgrade
```

The downside of introducing Flatpak, ie. introducing another package manager,
is that I'll need to learn new commands to update the software that comes via
this channel.

Fortunately, there's really not much to learn. From
[flatpak-update(1)](https://manpages.debian.org/unstable/flatpak/flatpak-update.1.en.html):

> flatpak update [OPTION...] [REF...]
>
> Updates applications and runtimes. [...] If no REF is given, everything is
> updated, as well as appstream info for all remotes.

Could it be that simple? Apparently yes, the Flatpak equivalent of the two `apt`
commands above is just:

```
$ flatpak update
```

Going forward, my options are:

1.  Teach myself to run `flatpak update` additionally to `apt update`, manually,
    everytime I update my system.
2.  Go crazy: let _something_ automatically update my Flatpak apps, in my back
    and without my consent.

I'm actually tempted to go for option 2 here, and I wonder if GNOME Software
will do that for me, given that I installed `gnome-software-plugin-flatpak`,
and that I checked « Software Updates -> Automatic » in the Settings.

However, I didn't find any documentation regarding what this setting really
does, so I can't say if it will _only download_ updates, or if it will _also
install_ it. I'd be happy if it automatically installs new version of Flatpak
apps.

So we'll see. Enough for today, hope this blog post was useful!
