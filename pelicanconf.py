#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Arnaud Rebillout'
SITENAME = 'Arnaud R'
SITEURL = ''
SITESRC = 'https://gitlab.com/arnaudr/arnaudr.io'

PATH = 'content'

TIMEZONE = 'GMT'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Biography
BIO = "Linux/FOSS software engineer"
PROFILE_IMAGE = 'avatar.jpg'

# Sidebar items
DISPLAY_PAGES_ON_MENU = False
MENUITEMS = (('Blog', SITEURL + '/'),
             ('About', SITEURL + '/about/'))
SOCIAL = (('email', 'arnaudr@debian.org'),
          ('linkedin', 'https://www.linkedin.com/in/arnaud-rebillout'),
          ('stack-overflow', 'https://stackoverflow.com/users/story/776208'))

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# URL settings
ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

# Theme
THEME = 'themes/pelican-hyde'

# Theme variables
COLOR_THEME = 'me'
FOOTER_TEXT = '''
Powered by <a target="_blank" href="https://blog.getpelican.com/">Pelican</a>
&bull; Source at <a target="_blank" href="{}/">GitLab</a><br>
&copy; 2015-2020 {}
'''.format(SITESRC, AUTHOR)

# Plugins
PLUGIN_PATHS = [ 'plugins' ]
PLUGINS = [ 'summary' ]
