That's my blog.


## Quick reminders for myself

For unfinished blog posts, set `Status: draft`

`planet-debian` is a special tag used to publish on <https://planet.debian.org/>


## Shortcuts

Here's a quick way to list `Categories`:

    grep --color=never --no-filename "^Category: " content/*.md | sort -u

Get the list of articles with a particular category?

    CAT=hacking; grep --color=never -l "^Category: ${CAT:?}.*" content/*.md

Here's a quick way to list `Tags`:

    grep --color=never --no-filename "^Tags: " content/*.md | sort -u

See all existing tags:

    grep --color=never --no-filename "^Tags: " content/*.md | \
      cut -d':' -f2- | tr ',' '\n' | sed 's/^ *//' | sort -u
