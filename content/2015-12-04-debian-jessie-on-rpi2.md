Title: Debian Jessie on Raspberry Pi 2
Category: sysadmin
Tags: debian, embedded, jessie, pi, raspberry, rpi2

How to install [Debian Jessie](https://www.debian.org/releases/jessie/)
on a [Raspberry Pi 2](https://www.raspberrypi.org/products/raspberry-pi-2-model-b/)
(aka RPi2), using the Debian image provided by [Collabora](https://www.collabora.com/).
<!-- PELICAN_END_SUMMARY -->

More info here: <http://sjoerd.luon.net/posts/2015/02/debian-jessie-on-rpi2/>.



## Installing Debian on the Micro SD-Card

Grab the Debian image files provided by Collabora,
thanks to the work of Sjoerd Simons.
You can find it here: <https://images.collabora.co.uk/rpi2/>.
Download all the files in the same directory.

In order to install it on the SD-Card, we need `bmap-tools`:

	apt-get install bmap-tools

We need a Micro SD-Card. I personnaly cleaned it completely with `gparted`,
I mean, removed every partition.
Then assuming the SD-Card is at `/dev/sdb` and is not mounted, run:

	$ bmaptool copy jessie-rpi2-20150202.img.gz /dev/sdb 
	bmaptool: info: discovered bmap file 'jessie-rpi2-20150202.img.bmap'
	bmaptool: info: block map format version 2.0
	bmaptool: info: 768256 blocks of size 4096 (2.9 GiB), mapped 123252 blocks (481.5 MiB or 16.0%)
	bmaptool: info: copying image 'jessie-rpi2-20150202.img.gz' to block device '/dev/sdb' using bmap file 'jessie-rpi2-20150202.img.bmap'
	bmaptool: info: 100% copied
	bmaptool: info: synchronizing '/dev/sdb'
	bmaptool: info: copying time: 40.7s, copying speed 11.8 MiB/sec

After that, we can have a look at it again with gparted, and mount partitions, just out of curiosity.
There's a small FAT16 partition for firmwares. It contains the kernel image, amongst other things.
The rootfs is written to the EXT4 partition.

It's the best moment to enlarge the rootfs partition if you feel you will need more space for the system.

I also created an EXT4 partition on the remaining space, that I will mount as `/home`. I took care to label it `home`.



## First steps with the RPI2

We're going to communicate with the RPI2 through an ethernet cable.
When booted, the RPI2 will send a DHCP request: it expects that a DHCP server somewhere will answer.
So you can either:

- plug the RPI2 to your router, and there's nothing to do (except to find which IP address the RPI2 received).
- plug the RPI2 directly to your computer. In this case, you must have a DHCP server up and running.

When the RPI2 is booted, we can connect with SSH. As mentionned, the default root password is `debian`.

	ssh root@192.168.1.11

Easy, wasn't it? Let's have a look around...

	# Have a look at the CPU
	lscpu
	# Get the temperature
	awk '{printf "%3.1f\n", $1/1000}' /sys/class/thermal/thermal_zone0/temp

Now, it's time to be serious. Let's change the root password:

	passwd

Change the hostname:

	echo 'rpi2' > /etc/hostname
	reboot

Generate new SSH keys (because SSH keys were embedded in the Debian image by mistake):

	rm /etc/ssh/ssh_host_*_key*
	dpkg-reconfigure openssh-server

Reconfigure time and locales:

	dpkg-reconfigure locales
	dpkg-reconfigure tzdata 

Let's tweak a little bit the root account to make it more comfortable:

	vi ~/.bashrc

Automount the `/home` partition.

	cat /proc/partitions
	echo '/dev/mmcblk0p3 /home ext4 relatime,errors=remount-ro,discard 0 2' >> /etc/fstab



## Upgrading Debian

At first, we must update Collabora keyring, which expired beginning of June,
and causes a `KEYEXPIRED error` when running `apt-get update`.

	$ apt-key list | grep collabora -A 4
	/etc/apt/trusted.gpg.d/collabora-obs-archive-keyring.gpg
	--------------------------------------------------------
	pub   2048R/B5FBB289 2013-05-09 [expired: 2015-06-01]
	uid                  Collabora OBS Automatic Signing Key <example@example.com>

I'm not good when it comes to security. So, it took me a while to find a solution,
and it may not be the right one. Anyway, I downloaded the latest keyring package from Collabora,
installed it with `dpkg`, and it did the tricks.

	wget https://repositories.collabora.co.uk/debian/pool/rpi2/c/collabora-obs-archive-keyring/collabora-obs-archive-keyring_0.5+b1_all.deb
	dpkg -i collabora-obs-archive-keyring_0.5+b1_all.deb

Then, we must give priority to Collabora repository. This is needed, otherwise official
Debian packages may replace the Collabora ones. This could be bad, since Collabora
brings what's currently missing in the official Debian to work on RPI2.
At the moment, Collabora provides 4 packages, and only one is also provided by Debian: `flash-kernel`.
You can `apt-pin` only this package, or the whole Collabora repository. I choosed the later option:

	echo '
	Package: *
	Pin: origin repositories.collabora.co.uk
	Pin-Priority: 1000
	' > /etc/apt/preferences.d/collabora

If you're curious, you can have a look at the Collabora packages that are currently installed, however you need `aptitude` for that.

	# List origins for packages that are installed
	apt-cache policy | sed -n 's/.*o=\([^,]\+\).*/\1/p' | uniq
	# List packages installed from a given origin
	apt-get install aptitude
	aptitude search '~S ~i (?origin("Collabora"))'
	i   collabora-obs-archive-keyring                   - GnuPG keys for the Collabora OBS repositories and archives
	i   flash-kernel                                    - utility to make certain embedded devices bootable
	i   linux-image-3.18.0-trunk-rpi2                   - Linux 3.18 for Raspberry Pi 2 model B+
	i   raspberrypi-bootloader-nokernel                 - Raspberry Pi bootloader


Now, we should add the security update repository.

	echo '
	deb http://security.debian.org/ jessie/updates main non-free
	' >> /etc/apt/sources.list

Alright, cross your fingers and invoke the almighty apt commands:

	apt-get update && apt-get dist-upgrade



## More stuff

Make yourself at home

	apt-get install bash-completion
	reboot
	apt-get install vim

Add the `sid` repository, pin it to a low priority, so that you can use it only for some packages.

	echo '
	deb http://ftp.debian.org/debian sid main
	' >> /etc/apt/sources.list
	echo '
	Pin: release a=unstable
	Pin-Priority: 100
	' > /etc/apt/preferences.d/sid

