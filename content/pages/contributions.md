Title: Contributions



## Goodvibes

I am the author and maintainer of [Goodvibes](https://gitlab.com/goodvibes/goodvibes),
a lightweight internet radio player.

- <https://gitlab.com/goodvibes/goodvibes/-/commits/master>
- techs: `c` `d-bus` `glib` `gobject` `gtk` `gstreamer` `meson`
- proj: `flatpak` `gitlab ci/cd` `readthedocs` `weblate`



## Debian

I am the maintainer of the Debian package [docker.io](https://packages.debian.org/sid/docker.io):

- <https://salsa.debian.org/docker-team/docker/-/commits/master>

You can also see my other contributions to Debian, mostly Go packages:

- <https://salsa.debian.org/users/arnaudr/contributed>



## Misc

CASync, various improvements:

- <https://github.com/systemd/casync/commits?author=elboulangero>
- techs: `c` `libcurl`

Pulseaudio, I worked on switching the build system from Autotools to Meson:

- <https://cgit.freedesktop.org/pulseaudio/pulseaudio/log/?qt=author&q=Arnaud+Rebillout>
- techs: `autotools` `c` `meson`

Quodlibet, I added Bash completion:

- <https://github.com/quodlibet/quodlibet/commits?author=elboulangero>
- techs: `bash` `python`

pgi-docgen (Python GObject Introspection), various improvements:

- <https://github.com/pygobject/pgi-docgen/commits?author=arnaud%40preev.io>
- techs: `gobject` `python`

Hyde theme for Pelican, the Static Site Generator:

- <https://github.com/jvanz/pelican-hyde/commits?author=elboulangero>
- techs: `css` `html` `jinja` `python`

