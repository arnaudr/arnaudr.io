Title: Ghost Theming
Category: blogging
Tags: git, github, ghost, themes

How to get and install themes for your [Ghost](https://ghost.org/) blog.
Then comes an example of Github-based workflow to customize your theme.
<!-- PELICAN_END_SUMMARY -->



## Getting and installing a new theme

This procedure is well described on a [Digital Ocean](https://www.digitalocean.com/) post:<br>
[How To Change Themes and Adjust Settings in Ghost](https://www.digitalocean.com/community/tutorials/how-to-change-themes-and-adjust-settings-in-ghost)

In two words:

- Find your theme. A good place to go is the [Ghost Marketplace](http://marketplace.ghost.org/themes/free/).
- Most of the time, themes are versioned on Github. So you can just clone
  them in your Ghost themes directory.

  	cd /srv/www/ghost/content/themes
  	git clone https://github.com/Skepton/Pixeltraveller.git

- Change ownership of the theme if needed.

  	chown -R ghost:ghost Pixeltraveller

- Then restart Ghost (this command depends on the service manager you use).

  	systemctl restart ghost

Now, just open your web browser, navigate yo your blog admin page.
In the `Settings > General` page, change the theme, and you're done.



## Customizing a theme - A Github workflow

After giving it some thoughts, I thought I could just go with the default theme,
and customize it a little bit.

You can edit the theme directly in your Ghost directory. It's OK if you just change a
thing or two, but if you want to make more changes, or play a little bit with it,
I recommend to version control the theme with Git.

The default theme (Casper) provided with Ghost is available on Github at this address:<br>
<https://github.com/TryGhost/Casper>

If you have a Github account, the best thing to do is to fork the Casper theme, then clone
your fork inside your Ghost directory. Be sure to give the repository another name (here
`casper-git`) so that you can have both theme available, the default and the one you're
working on.

	cd /srv/www/ghost/content/themes
	git clone git@github.com:elboulangero/Casper.git casper-git

Of course, replace the URL above by the one of your fork.



## Working locally on your theme

You can work directly on the VPS that hosts the Ghost blog, but soon
you will find that it's not practical.

- you probably don't want to test things on a blog that is published, while
  people are looking at it, do you?
- it's way faster to work locally than on a remote server (depends on your
  connection of course, but mine sucks)
- sometimes, it's good to have a GUI (I'm a big fan of `gitk`).

So you need to setup a Ghost blog locally on your machine. It's not so hard,
just follow the same steps than you did to install Ghost on your server,
except that there's no need to install Ghost in `/var/www`, you can just
put it in your own directory. No need to change ownership too.

Then, just `git clone` your theme inside Ghost, as you did before.

Create a few posts, and start playing with the theme. Serve the blog
locally with `npm start`, look at your changes, make yourself at home.

Once you're happy with your changes, `git push`.



## Syncing the theme with your server

There are several ways to do that.

I personnally do that manually. Both on my server and on my laptop, I have my Ghost
theme *git-cloned* inside the Ghost directory.

I work on the theme on my laptop, `git push` the changes when I'm done. Then I ssh to my
server, I `git pull` the theme, restart Ghost and have a look at it on my real blog.
Often, I may find little details to fix that I didn't see before, and at this moment I find
it more convenient to fix it directly on the server. So I do my last changes here,
then I can `git push` from my server.

This is my own way of doing it, it has the advantage that I can work from both my laptop
and my server, but has the disadvantage that I have two concurrent git users working
on it, so I can mess a little bit with git if I'm not careful.

But if you google around a little bit, you'll see that there's another common workflow
that involves a Git hook server-side. The idea is to hook Git so that it deploys your
changes automatically each time you push. The advantage is that it's all automated with Git.
But it can also be a disadvantage, maybe you don't want that. Furthermore, if you version
your theme with Github, you can't setup a hook server-side.

You could also synchronize the theme with a rsync command if you like it, that's just
another way of doing it...

