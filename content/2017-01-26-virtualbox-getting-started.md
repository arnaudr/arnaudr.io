Title: VirtualBox In Five Minutes
Category: dev
Tags: virtualbox

Getting started with [VirtualBox](https://www.virtualbox.org/) in a few words...
<!-- PELICAN_END_SUMMARY -->


## Install

It all starts here.

	apt-get install virtualbox

Then, creating image just boils down to clicking on "Next" a couple of times.
Soon enough you will create image after image without even giving it a look.



## Sharing The Clipboard

Soon you will want to copy/paste like crazy from host to guest, and for that you will need the guest additions.

On the guest, install, then reboot.

	apt-get install virtualbox-guest-dkms
	reboot

Then just enable this wonderful feature in the machine settings: "Devices -> Shared Clipboard". 

If for some reason it doesn't work, check this page for a complete procedure:<br>
<http://askubuntu.com/q/22743/424636>



## Sharing Directories

#### Automatically With VirtualBox "Auto-mount"

Now, you might want to share some data: "Shared Folders" is up to the task.
"Devices -> Shared Folders -> Shared Folders Settings", click the button "Add",
then don't forget to check "Auto-mount" and "Make Permanent".

The folders show up under `/media/`. In order to access it, you must add yourself to a group:

	sudo adduser <user> vboxsf

Don't forget to log out and in for the change to take effect.

Now, that was very straightforward, but it's not completely satisfactory.
At first, if you're like me and you don't like to leave your home directory,
then you won't like to have to navigate to `/media/` each time.
Second (and more important), the files belong to root, and if you care a bit
about ownership, and if you're a clean person, then you won't like it.

To solve the first problem, you can use a bind mount, like that:

	/media/<sf_shared_dir> /home/<user>/<dirname> none bind

But that won't solve the ownership issue.

#### Automatically, But Smarter

So, the cleanest way is actually to disable the "Auto-mount" setting for your shared folder,
and setup the automation yourself. It's fairly easy.

The most obvious solution it to add a line to `/etc/fstab`:

	<shared_dir>	/home/<user>/<dirname>	vboxsf	uid=1000,gid=1000	0	0

However in my case it failed the boot, probably because it was exectued too early or something.
So instead, I add to put the mount command in `/etc/rc.local`, and to delay it a bit.

The cleanest way for me to do that: keep the line in `fstab`, make it `noauto`,
then mount from `rc.local` with a delay.

	# That's /etc/fstab
	<shared_dir>	/home/<user>/<dirname>	vboxsf	noauto,uid=1000,gid=1000	0	0
	# That's /etc/rc.local
	(sleep 5s && mount <shared_dir>) &

Notice that `<shared_dir>` is the folder name you gave to VirtualBox when you
created the shared folder. It acts as an uid here.



## In a nutshell

So, to finish, here are the commands I run on every guest after a fresh install.
I know you don't give a damn, but I do, because this blog is my second memory.
That's what it's called a memento :)

	sudo dpkg-reconfigure tzdata
	sudo apt-get update && sudo apt-get -y dist-upgrade
	sudo apt-get -y install virtualbox-guest-dkms
	sudo apt-get -y install qdbus git gitk vim
	# And so on, add your favorite packages here...
	# When it's over, save a bit of space?
	sudo apt-get clean

