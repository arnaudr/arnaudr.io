Title: Getting Started with Apache
Category: sysadmin
Tags: apache

A quick introduction to [Apache](http://httpd.apache.org) for the noobs.
<!-- PELICAN_END_SUMMARY -->



## Introduction

I host this blog on a [Debian](https://www.debian.org/) VPS.
So at some point I had to setup a web server,
otherwise how could you be reading this blog right now?

There are many web servers around, some of the most populars being
[Apache](http://httpd.apache.org), [Nginx](http://nginx.org) or
[Lighttpd](http://www.lighttpd.net), to cite only a few.
I never used Apache before, so I thought it was the opportunity to fill this gap.



## Installation

Installing is nothing fancy:

	apt-get install apache2

Right now Apache is configured and ready to serve.
If you have a look at your server through your web browser,
you should see the default Apache page for Debian.



## Basic Configuration

Now, let's create a new site. A basic site definition looks like that:

	echo '
	<VirtualHost *:80>
		DocumentRoot /srv/www/blog-test
	</VirtualHost>
	'>  /etc/apache2/sites-available/blog-test.conf

Notice that I like to put my websites in `/srv/www`.
On Debian, the default Apache configuration only allows serving websites
from `/var/www`. To allow `/srv/www` (or whatever suits you), edit the file
`/etc/apache2/apache2.conf`. Look for `/srv`.
The configuration is already there, you just have to uncomment it.

Then, we disable the default site served by Apache, and enable our site:

	a2dissite 000-default
	a2ensite blog-test
	systemctl restart apache2

And that's it! Now Apache serves the data from `/srv/www/blog-test`.
So you should copy your website in this location. Then, don't forget
to change the ownership for this directory. On Debian, Apache is configured
to run as `www-data`.

	cp -r path/of/your/website/* /srv/www/blog-test
	chown -R www-data:www-data /srv/www/blog-test

