Title: Publishing A Debian Package (Mentors & Sponsorship)
Category: dev
Tags: apt, debian, mentor, packaging, sponsorship, pnmixer

Here are the steps to follow to publish a Debian package,
so that it becomes part of the official Debian repositories.
<!-- PELICAN_END_SUMMARY -->



## Introduction

Here comes the second part of our [Debian](https://www.debian.org/) packaging story!

This is a follow-up of the post [First Debian package]({% post_url 2016-01-14-debian-package-starting %}).

Alright, let's assume that you have a Debian package in your hands. It's nice and all,
and now you wish to make it available to the world. That is, you wish
that your package make it to the official Debian repositories. Here are the steps to do that.

Of course, I assume that you're not a DD (Debian Developer), otherwise you
wouldn't be reading that. No no no, you're just a random guy like me.
How can you get your package to be accepted by the Debian team?

Well, basically, you will have to find a Debian Developer who is willing to
help you. He will review your package, tell you what needs to be fixed,
and in the end accept your package. This guy is called a __mentor__,
and what you'll be doing is a __request for sponsorship__.

For this example, we use the [PNMixer](https://github.com/nicklan/pnmixer) package,
version 0.6.1.



## Installing

There's one MUST HAVE tool in this story: `reportbug`.

	apt-get install reportbug

There's a bit of configuration the first time you run it, don't be scared :)

You will also need `debsign` to sign the package:

	apt-get install devscripts

And at last, the package upload tool:

	apt-get install dput



## References

There is plenty of documentation available on the Net already.
Our main source of information here is Debian Mentors pages.
Please ensure you read that all.<br>
- <http://mentors.debian.net>
- <http://mentors.debian.net/intro-maintainers>

There is also the Debian Mentors FAQ, in the Debian Wiki.
The best "in a nutshell" guide is provided in the last link.
The 6 steps described here sum it all up.<br>
- <https://wiki.debian.org/DebianMentorsFaq>
- <https://wiki.debian.org/DebianMentorsFaq#How_do_I_make_my_first_package.3F>



## Get to know the tools

First of all, Debian maintains a list of "work in progress" packages,
called the __WNPP__ (Work-Needing and Prospective Packages).
Your package will make it to this list at first.

To get started, please visit: <https://www.debian.org/devel/wnpp/>.

Second, you will have to deal with the __Debian BTS__ (Bug Tracking System).
You will basically send mails to this robot in order to add your package
to the WNPP list, and change the status of your package if needed.

The Debian BTS starting point: <https://bugs.debian.org/>. From there,
there are two pages that will teach you how to communicate with the server:
- <https://www.debian.org/Bugs/server-request>
- <https://www.debian.org/Bugs/server-control>

There's one thing to understand here: `wnpp` is a pseudo-package.
And like every package, you file bug reports against it. And this is exactly
how it works: when you want to add your pacakge to this list, you file a
bug report. When you want to remove it, or change the status, you file a
bug report. But these bug reports have special format and keywords, so
that they can be parsed and handled by the Debian BTS.

To ensure that you don't mess up with the content of these bug reports,
you use `reportbug` to create your bug reports. This tool takes care
of the formatting, and in the end you just have to fill it a formatted
mail. Pretty cool, I tell you.



## Deal with the WNPP

OK, so before everything else, the first thing to do is to inform the Debian
community that you're working on a package. You do that by filing a bug against
the `wnpp` pseudo-package on the Debian BTS.

Have a look at the WNPP list by visiting this page: <https://bugs.debian.org/wnpp>

Be patient, the page is a bit long to load. There are a lot of packages there.

When the page is ready, do a quick search to see if the software you're packaging is
mentioned in this list.

### Retitle an RFP to ITP

When I packaged PNMixer 0.6, PNMixer was already mentioned in the list as an
__RFP__ (Request For Package). In such case, all you have is to do is to ask
the BTS to retitle the package into an __ITP__ (Intend To Package).

I did that simply by sending the following email to `control@bugs.debian.org`:

	retitle 745669 ITP: pnmixer -- Volume mixer for the system tray

### File an ITP

Otherwise, if your package is not mentioned in the WNPP list, 
and doesn't already exist in Debian, then you have to
file an ITP bug report against the `wnpp` pseudo-package.

The right tool to do that is `reportbug`. 

	$ reportbug wnpp
	...
	Choose the request type: 1
	Please enter the proposed package name: pnmixer
	Checking status database...
	A package called pnmixer already appears to exist (at least on your system); continue? [y|N|q|?]? y
	Please briefly describe this package... : Simple mixer application for system tray
	Your report will be carbon-copied to debian-devel, per Debian policy.
	Querying Debian BTS for reports on wnpp (source)...

From then, just press _Enter_ to discard the thousands of bug reports displayed.

Then, you're taken to your usual console text editor, and it's time to fill the blanks
(version, upstream author and so on). In the end, my email would be something like that:

	Subject: ITP: pnmixer -- Simple mixer application for system tray
	Package: wnpp
	Owner: "Your Name" <your@name.com>
	Severity: wishlist
	
	* Package name    : pnmixer
	  Version         : 0.7
	  Upstream Author : Nick Lanham <https://github.com/nicklan>
	* URL             : https://github.com/nicklan/pnmixer
	* License         : GPL
	  Programming Lang: C
	  Description     : Simple mixer application for system tray
	
	PNMixer is a simple mixer application designed to run in your system
	tray. It integrates nicely into desktop environments that don't have
	a panel that supports applets and therefore can't run a mixer applet.
	In particular it's been used quite a lot with fbpanel and tint2, but
	should run fine in any system tray.
	
	PNMixer is designed to work on systems that use ALSA for sound
	management. Any other sound driver like OSS or FFADO are currently not
	sup supported (patches welcome). There is no *official* PulseAudio 
	support at the moment, but it seems that PNMixer behaves quite well
	anyway when PA is running. Feel free to try and to give some feedback.

Save and quit, then one last confirmation is required...

	Report will be sent to "Debian Bug Tracking System" <submit@bugs.debian.org>
	Submit this report on wnpp (e to edit) [Y|n|a|c|e|i|l|m|p|q|d|t|s|?]? y
	Saving a backup of the report at /tmp/reportbug-wnpp-backup-20161001-5679-xi8Z2N
	Connecting to smtp.gmail.com via SMTP...
	
After a few minutes, you should receive a confirmation from the server.
This is precious, because it containt the ITP bug number, and we need that in the next step.



## Ensure your packages closes the ITP

In the case you submitted an ITP bug report, then you must ensure that your package
closes this bug report. This is something you document in the Changelog.

So, back in your package, edit the file `debian/changelog`. If it's the first
version of the package you should have a line like that:

	  * Initial release. Closes: #nnnn.

Replace `#nnnn` with the bug number of your ITP.

Otherwise, add such a line (replace `#nnnn` of course):

	  * Closes: #nnnn (ITP).

Then rebuild your package.

	dpkg-buildpackage -us -uc



## Create a GPG key

If you don't have a GPG key, it's time to create one.

	gpg --gen-key

GPG puts its stuff in your home directory:

	ls ~/.gnupg/

You can list keys:

	gpg --list-keys

And that's enough for the moment...



## Sign the package

Now, that you have a GPG key, signing the package is simple.

	debsign pnmixer_0.6.1-1_amd64.changes

What's important here is that the identity you used to make the package
(ie your name and email) must be exactly the same as the one you used
to create the GPG key. Otherwise you will get an error like that:

	gpg: skipped "Your Name <your@mail.com>": secret key not available

If you're in this situation, you can recreate a new GPG key with the right name,
or you can also use the `-m` option of `debsign` to give another identity.



## Upload to Debian Mentors

Create an account on <https://mentors.debian.net> (account type: maintainer).

Validate your email address. Login to your account. Add your GPG key.
To have a public version of your GPG key, use the command suggested by the website.
At the moment, it is:

	gpg --export --export-options export-minimal --armor \
		"Your Name <your@mail.com>" \
		> ~/.gnupg/yourname.pub

And now, you should upload your signed package to the website using `dput`.

If you just installed `dput` at the beginning of this tutorial, and never run it,
then there's a bit of configuration to be done. You must create a configuration file.
Check <http://mentors.debian.net/intro-maintainers> for an up-to-date version of that.

	echo '
	[mentors]
	fqdn = mentors.debian.net
	incoming = /upload
	method = http
	allow_unsigned_uploads = 0
	progress_indicator = 2
	# Allow uploads for UNRELEASED packages
	allowed_distributions = .*
	' > ~/.dput.cf

Then, a simple command will do:

	$ dput mentors pnmixer_0.6.1-1_amd64.changes
	...
	Uploading to mentors (via http to mentors.debian.net):
	  Uploading pnmixer_0.6.1-1.dsc: done.
	  Uploading pnmixer_0.6.1.orig.tar.gz: done.    
	  Uploading pnmixer_0.6.1-1.debian.tar.xz: done. 
	  Uploading pnmixer_0.6.1-1_amd64.deb: done.    
	  Uploading pnmixer_0.6.1-1_amd64.changes: done.
	Successfully uploaded packages.

You should receive a confirmation email after a few minutes.



## Check your package on Debian Mentors

Just connect to your personal page on Debian Mentors:
<https://mentors.debian.net/packages/my>.

Your package will show up after you receive a confirmation email
for your upload. It takes a bit of time, so just be patient,
relax and have a drink in the meantime.

This page will display everything that is right or wrong with your package.
If you've been serious and checked it properly with `lintian` beforehand,
you should have no bad surprise. Otherwise, you might discover that your
package is not as good as you thought...



## Ask for a sponsorship

This is the last step :)

It happens on the Debian Mentor page. Have a look at the details for your package.
For me, it happens here: <https://mentors.debian.net/package/pnmixer> (this page
is probably unavailable at the moment you read these lines).

Among the several details, the one that matters to us is __Needs a sponsor__.
Enable this, then have a look at the home page of Debian Mentors:
<https://mentors.debian.net/>. Your package should show up here. Great, isn't it?

And now, the very very last step. You should explictly utter a call for sponsorship.
The procedure is described on your package page in Debian Mentors. Just click
the __View RFS template__ link (RFS stands for Request For Sponsorship).

Everything is explained here. In a nutshell, it's just filing a new bug report
against the `sponsorship-requests` pseudo-package.

You can see all the pending sponsorship-requests by visiting the page:
<https://bugs.debian.org/sponsorship-requests>. It will provide you tons
of examples of RFS mails.

To file the bug report, you can use the usual tool `reportbug`.
Although at the moment of this writing, there's no reportbug template
for the `sponsorship-requests` pseudo-package.

So it might be easier to just use your usual mail client, and copy/paste
the template given by Debian Mentors.

I personally did that with `reportbug`, so here comes the procedure.

	$ reportbug sponsorship-requests
	...
	A package named "sponsorship-requests" does not appear to be installed... n
	Getting available info for sponsorship-requests...
	Please enter the version of the package this report applies to (blank OK)
	> 
	Will send report to Debian (per lsb_release).
	Querying Debian BTS for reports on sponsorship-requests (source)...
	...

Just discard the bug reports found by hitting _Enter_. Then it starts to be interesting.
From now on, you should use the template provided by the Debian Mentors, and mentioned
above.

	Briefly describe the problem (max. 100 characters allowed).
	> RFS: pnmixer/0.7-1 [ITP] -- Simple mixer application for system tray
	...
	Please select a severity level: [normal] 6
	...
	Please select tags: (one at a time) [none] 

Then it's time to edit the mail with your favorite console text editor.
This is when you basically copy/paste the template provided by Debian Mentors,
and discard everything reportbug prepared for you :)

Save and quit, then one last confirmation is required...

	Report will be sent to "Debian Bug Tracking System" <submit@bugs.debian.org>
	Submit this report on sponsorship-requests (e to edit) [Y|n|a|c|e|i|l|m|p|q|d|t|s|?]? y
	Saving a backup of the report at /tmp/reportbug-sponsorship-requests-backup-20161001-10960-bJE9KK
	Connecting to smtp.gmail.com via SMTP...

Done!

Now you just wait and see!



## Follow-up

When your package has been accepted by Debian, there are several places that you can visit
to have a look at your package. Here they are (for the PNMixer package of course).

- <https://tracker.debian.org/pkg/pnmixer>
- <https://packages.debian.org/pnmixer>
- <https://bugs.debian.org/pnmixer>
