Title: Unbloating The Sony Xperia M4 Aqua 8GB
Category: hacking
Tags: android, sony, xperia, m4, aqua, twrp, supersu, bloat

Sony Xperia M4 Aqua (E2353). A promising mobile phone, mid range,
not too expensive, dustproof and waterproof.
The only problem is the storage. Some comes with 16GB (lucky you),
but mine has only 8GB. And this is not enough!
<!-- PELICAN_END_SUMMARY -->

Out of the box, after updating everything, there's only 1.26GB free.
After using it a few weeks, I'm constantly hit by the message about
the memory being full. In fact, the phone proves to be a pain...

For a better and longer writing about this problem, have a look at:<br>
<http://www.xda-developers.com/8gb-sony-xperia-m4-aqua-comes-with-only-1-26gb-free/>

At first, I thought I could reflash it with a clean CyanogenMod image.
This would free my phone of all the Sony bloatware, and save some precious memory I'm sure.
But at the moment of this writing, the best I could find is an unofficial ROM,
and it's still under development. Some things don't work yet.
For those interested it's available here:<br>
<http://projectmarshmallow.altervista.org/unofficial-cyanogenmod-12-1-for-sony-m4-aqua/>

So I thought the best was to stick with the official Sony firmware,
and to uninstall all the bloatware. But you can't do that out of the box.
The stock apps can't be uninstalled, only disabled. They still use memory.
The reason, as far as I understand, is quite simple. Stock apps are installed
on the system partition, and this partition can't be modified without having root access.
For a quick overview of Android partitions scheme, refer to:<br>
<http://www.addictivetips.com/mobile/android-partitions-explained-boot-system-recovery-data-cache-misc/>

Since I'm a very newb with smartphone in general, and Android in particular,
it took me quite a while to understand the whole procedure needed to remove
this stock apps. I loosely followed this tutorial:<br>
<http://forum.xda-developers.com/m4-aqua/general/tut-blu-twrp-root-xperia-m4-aqua-t3238962>

But it doesn't give any explanations about *why* we do the things, so I dug
further to get to understand it properly. And now I'm able to sum that up in a few words!
And here they are...



## Edit October 2016

Sony has now released a firmware based on Android 6 (aka Marshmallow):
- <http://www.ibtimes.co.uk/xperia-m4-aqua-m5-getting-android-6-0-1-marshmallow-update-1571792>
- <http://support.sonymobile.com/global-en/xperiam4aqua/kb/8019307466b15f3d01556b8631c4004c80/>

With Marshmallow comes a feature named [Adoptive Storage](http://www.greenbot.com/article/3039136/android/adoptable-storage-in-android-6-0-what-it-is-how-it-works.html),
which allows you to use part of your SD-Card as system storage. It's all transparent to the system,
so it's really an extension of the internal memory.

Although this feature doesn't come natively with the Sony ROM, it's possible to use it anyway.
There's more info about that here:<br>
<http://forum.xda-developers.com/m4-aqua/general/adoptive-storage-marshmallow-t3421828>

It looks like this is so far the best solution for the M4 storage problem.
If I have time to go through these steps I'll write a post about that.

Thx to Bryden for giving me the info.



## Introduction

In this tuto, we're going to rid our Xperia M4 to its pre-installed software.

In order to do that, we need several things:

- an uninstaller app: [System app remover (ROOT)](https://play.google.com/store/apps/details?id=com.jumobile.manager.systemapp).
  This app needs to have a root access on the device, and for that we need another app...
- a superuser access management app: [SuperSU](https://play.google.com/store/apps/details?id=eu.chainfire.supersu).
  This app can't be installed through the Google Play Store, since it needs to modify the system partition of Android.
  Therefore, it needs to be installed from the recovery software. And the stock recovery software doesn't allow that.
  So we need to install a custom recovery software.
- a custom recovery software: [TWRP](https://twrp.me/). However, in order to be able to modify the recovery partition,
  we need...
- unlocking the bootloader.

Here you can see clearly the VERY long and complicated path that we're about to follow, simply to uninstall a few apps...

But cheer up, it's not that hard to do, and every Android hacker has to start from here I guess.
There are hundreds of tutorials on the net, but most of them are very quickly written,
and only describe the steps without explaining much. But I like to speak and explain stuff,
so hopefully here you'll get to understand what you're doing.

So we're gonna do all these things to your Android device. I have a Sony Xperia M4 Aqua,
but I guess the same steps work more or less for most of the Android devices.
You just have to ensure, each time, that you're device is compatible with the tools you're using.

As usual, I assume you're having a GNU/Linux computer in front of you, and it's Debian powered,
but any other distro can do, there's nothing Debian specific here, except for package manager.



## Warnings

Following this tutorial will wipe your phone. That means a factory reset.
You will loose all your data, and your phone will be brand new.

Also, this procedure will rip you off your device's warranty.

You can't held me responsible for that.
I warned you, I assume you know what you're doing.

You can keep on reading now!



## Unlock the bootloader

At first, you should know, right now, that unlocking the bootloader will wipe your phone.
Erase all the data. That's it. It may not be obvious at first, since there seem to be
no relation between the bootloader and the data on your phone. However, from a security
point of view, it makes sense. Here's a good discussion about that:<br>
<http://android.stackexchange.com/q/33691>

Ok, now, let's get started with this first step.
As usual, we start by installing everything we need.

        apt-get install android-sdk

Amidst the Android SDK are some tools that we will need:
`fastboot` to send commands to the Android bootloader,
and `adb` to send commands to the booted Android device.

Ok, so most of this procedure happens in your web browser.
You need a code to unlock your device.
Visit this weppage from Sony and follow the steps, it's straight-forward.<br>
<http://developer.sonymobile.com/unlockbootloader/unlock-yourboot-loader/>

To sum up what happens:

- choose your Xperia model (M4 Aqua)
- check your mails, click the confirmation link
- get the IMEI of your phone by dialing `*#06#`
- feed that to the webpage
- now you get an unlock code!

The following steps are clearly detailed on Sony website, except for one little details, so read on!

On your device, at first, you can check whether the bootloader *can* be unlocked.
This is done by dialing `*#*#7378423#*#*`. This opens the `Service Menu` (it may take a few seconds).
In here, go to `Service Info -> Configuration` and check the value of `Rooting status`.
You should have that: `Bootloader unlock allowed: Yes`

Ok, so now there's two things to do on the phone to prepare it for unlocking.

First, turn on USB debugging by going to `Settings > Developer options` and click to enable USB debugging.
As of Android Jelly Bean 4.2 the Developer options are hidden by default.
That's why you don't see it anywhere...
To enable them tap on `Settings > About Phone > Build Version` multiple times.
Then you will be able to access the `Settings > Developer options`.

Second, allow OEM unlock (it's the missing bit in the Sony tutorial) by going to
`Settings > Developer options` and clicking `OEM unlocking`.

Now, turn off your Xperia M4 Aqua.

Press (and keep pressed) the **volume up** button, and connect the device to your computer with the USB cable.
This prevent the device from starting normally, instead it will just sit in the bootloader mode and wait.
The LED next to the USB connector turns red, then blue, and remains blue like that.

Check that the device is recognized and ready to communicate by running this command:

	$ fastboot devices
	AB123CDEF4      fastboot

Now enter the unlock key to unlock the bootloader.

	$ fastboot -i 0x0fce oem unlock 0x<your-unlock-key>
	...
	OKAY [  4.487s]
	finished. total time: 4.487s

Done! Start your phone, it's fresh as if it was out of the factory!
 


## Install TWRP

Now that the bootloader is unlocked, we will be able to install a custom recovery software.

If you're wondering what the hell is that, you may have a look here:<br>
<http://geekwarning.com/mobiles/twrpcwm-recovery-on-sony-xperia-m4-aqua/>

We need a custom recovery software to install a superuser app.
There seem to be many custom recovery for Android. We have to pickup one of them.
We're gonna install TWRP, as it seems to be fairly popular.

At the moment of this writing, there's no official support for the Xperia M4 in TWRP.
However, there's a beta that is reported to work well. It's available here:<br>
<http://projectmarshmallow.altervista.org/twrp-sony-m4-aqua/>

A discussion and feedback on this beta is available here:<br>
<http://forum.xda-developers.com/m4-aqua/development/recovery-twrp-touch-recovery-sony-m4-t3229873>

Download the TWRP image.

Ensure the USB debugging is enabled on your M4.

Reboot you Xperia M4 in bootloader mode. You know how to do that by now, right?
Turn off the device, then press the volume up button and plug it to your computer.

Here's another way to do that. Assuming your device is already up and running,
and is connected to your computer, just type in a terminal:

	adb reboot bootloader

Now that the device is sitting in bootloader, install TWRP:

	$ fastboot flash recovery recovery.img
	target reported max download size of 268435456 bytes
	sending 'recovery' (24640 KB)...
	OKAY [  0.775s]
	writing 'recovery'...
	OKAY [  1.087s]
	finished. total time: 1.862s

(I assume here that the TWRP image you downloaded is named `recovery.img`,
and that it's in the current directory)

That's it! Done already! You replaced the stock recovery software by a custom one: TWRP.

You can reboot the phone with the command:

	fastboot reboot



## Install SuperSU

And now we need to install a superuser management app, and it seems that the most popular
out there is SuperSU. So let's go for it.

A good tutorial for that:<br>
<http://www.howtogeek.com/115297/how-to-root-your-android-why-you-might-want-to/>

A few words on what is Super SU:<br>
<http://www.android.gs/install-supersu-on-android-devices-to-improve-superuser-access/>

Basically, it's what we need to have root access on the device, and to provide root access
to other third-party apps.

SuperSU is available from the Google Playstore, so you could install it like any other app,
however if you do so you will only have the frontend. The backend (the `su` command)
can't be installed that way, it needs to be installed using a custom recovery software.

Discussion and explanation about that:<br>
<http://android.stackexchange.com/q/96736>

So, at first, download the SuperSU zip archive from this page:<br>
<https://download.chainfire.eu/supersu>

Now, let's boot your Xperia M4 in recovery mode. To do that,
I had to turn off the Xperia, then turn it on by holding **power button**
and **volume down** button simultaneously. Doing that, the phone boots in
recovery mode, and we get a nice screen from TWRP.

On the first run, TWRP asks you if you want to run in read-only mode. You can say yes.

Now, connect the device to your computer with USB cable.

From your computer, mount the Xperia M4 device if needed.
You should be able to see the Xperia in your file browser,
assuming that the MTP is properly installed on your computer...

Now, just copy the SuperSU archive (`UPDATE-SuperSU-v2.65-20151226141550.zip` at the moment of this writing)
somewhere on your Xperia M4.

Then, on your Xperia M4, currently running TWRP, choose `Install`, navigate in the directories to where you copied the zip,
and choose to install it.

Done! SuperSU is installed!

Now you can reboot the device, and you'll see that SuperSU has been added to the apps.



## Cleaning the Xperia M4

We're almost there, be patient. Now, we need to install an application that allow to uninstall
the bloatware. I choosed `System app remover (ROOT)` since it was well ranked in the Play Store.

After that, launch it, and you will see that there are many many things that are installed.

What can be safely removed? Hard to say, it depends on what apps you don't need,
and how reckless you are with the system app that you have no clue about...

There's a fairly good list available here:<br>
<http://forum.xda-developers.com/z3/general/apps-safe-to-uninstall-z3-t3019684>

And a more advanced discussion, with a longer and more reckless list, here:<br>
<http://forum.xda-developers.com/crossdevice-dev/sony/wip-debloat-lp-690-correct-functioning-t3076161>

Before doing anything foolish, I backed up my fresh Android on the SD-Card with TWRP.
You should do the same, since now we start doing things that may break the system.
Well, it's no big deal, I guess you can always re-install a clean Sony Xperia M4 firmware
if you mess up the phone. But backing up with TWRP saves time.

Ok, anyway, here's my list of apps removed. Yours may be different.

- Active Clip
- Ant HAL Service
- AR effect
- AR fun
- Backup & restore
- Black Hole
- Bubbles
- Calculator (Small Apps)
- Chrome (I use Firefox)
- Clocks
- Creative effect
- DrmDialogs
- Enchanted forest
- Enterprise Single Sign On
- Exchange Services
- File Commander
- Google lyrics extension
- Google Play Movies
- Google Text-to-speech Engine
- Hangouts
- Live Wallpaper Picker
- Lounge
- Movie Creator
- Multi Camera
- Music Visualization Wallpapers
- my Xperia
- OfficeSuite
- OMA Client Provisioning
- Oma Download Client
- OmaV1AgentDownloadService
- Phase Beam
- Portrait retouch
- Retail Demo
- Setup guide
- Setup Wizard
- Simple Home
- Sketch
- Small App *
- Software update
- Sound Photo
- Street View
- Support
- TalkBack
- Timer
- Top Contacts provider
- Touch Block
- Trusted Face
- Video
- Video Editor
- Visualiser
- What's New
- Wikipedia extension
- Xperia Calendar sync
- Xperia Music Likes
- Xperia Social Engine Photos
- Xperia camera add-ons
- Xperia Chinese keyboard
- Xperia extension for Throw
- Xperia Lounge Pass
- Xperia wallpapers
- Xperia Web Runtime
- Xperia with Facebook
- Xperia with Twitter
- Youtube extension
- Youtube karaoke extension

Also, in the app remover, you can sort apps by path. Then you can remove anything with a path
starting with `/system/vendor/overlay/` and `/system/vendor/app`.



## Conclusion

After removing all that things, how much space did I gain? Around 250MB.

Wait...  What? 250MB??? All that efforts for so little result?

Yeah, that's a bit disappointing... Well we can rejoyce by remembering that we also saved some RAM,
therefore some CPU, therefore some battery life. But how much exactly, no idea. Probably very little.

I guess now, the best hope for a really lighter Android install would be to have
a custom rom to install. An official CyanogenMod would be great. I'll keep an eye on that.

In the meantime, I'll keep the number of apps installed as small as possible,
and throw as much as I can on the SD-Card. If it's not enough, I'll probably
give a try to [Links2SD](https://play.google.com/store/apps/details?id=com.buak.Link2SD).

Cheers!

