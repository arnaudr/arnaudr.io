Title: Rendering Markdown To PDF
Category: hacking
Tags: bash, markdown, pdf

When I have stuff to write, I like to keep it simple. It means plain text, editable with simple, dumb editors. As long as I can stay away from word processors, I do.
<!-- PELICAN_END_SUMMARY -->

With time, I found myself increasingly respecting the markdown syntax when I write. First, because having a consistent syntax doesn't harm. And second, because it allows to easily render the text to something a bit more appealing, with italic, bold, titles, and so on.

So, lately, I was writting some stuff for someone else. Could I send him the markdown file directly? I thought I could do better, and send him a formatted version. And I immediately thought: PDF.

So, how do we do that? Looking around, it appears that there are different solutions. The good starting point in your research is on Superuser at the moment:<br>
[How Can I Convert Github-Flavored Markdown To A PDF](http://superuser.com/q/689056)

It seems that most solutions (maybe all of them) are a two-steps process: first you render your Markdown to HTML, then you convert the HTML to PDF. There are a variety of tools for each task, which gives you some room to experiment. You could use the simple `markdown` tool for the first step, then a tool like `wkhtmltopdf` for the second.

Or you can use an all-in-one tool. After a few trials, I settled for this latest option, and opted for [markdown-pdf](https://github.com/alanshaw/markdown-pdf).

No Debian package, the best is to install it through the NodeJS package manager. Here we go. Make sure you have NodeJS already installed on your system.

	sudo npm install -g markdown-pdf

Converting boils down to a single command. It's a zero hassle solution, but also an almost zero customisation. I was happy with the result, mainly because the page break was smart enough not to happen in the middle of a paragraph. That's all, but still, `wkhtmltopdf` didn't had such a smart break policy.

To automate the conversion of my bunch of Markdown files, I wrote down a simple Makefile.

```makefile
MD=$(wildcard *.md)
PDF=$(patsubst %.md,%.pdf,$(MD))

all: $(PDF)
%.pdf: %.md
        markdown-pdf $^

.PHONY: clean
clean:
        rm -f *.pdf
```

Now it all boils down to `make` and `make clean`. Pretty cool, isn't it?

