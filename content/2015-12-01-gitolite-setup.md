Title: Setting up Gitolite
Category: sysadmin
Tags: bash, git, gitolite

How to install and setup [Gitolite](http://gitolite.com) on a remote server,
using the upstream source.
<!-- PELICAN_END_SUMMARY -->



## Introduction

I'd like to point out that the official Gitolite documentation
is very well written, more than complete, and constantly updated.
You will find everything you need and more.
If you're new to Gitolite, you'd better learn from the official doc, this is my advice.

I may also mention that there is a [Debian](https://www.debian.org/) for Gitolite.
I tried it, but in the end I prefered to install Gitolite from upstream.
Mainly because:

- Debian's default location for repositories is at `/var/lib/`.
  I like it better at `/home`, since this partition is usually much bigger
  (at least on my setup), and is the perfect place to hold data.
  This default location is said to be changeable through `dpkg-reconfigure`,
  but I'm not sure it works that well, I saw some tickets about that.
- The Debian package is a little bit outdated at the moment.
- There are some unresolved tickets lingering for a while, and that makes me wary.

In the end, I felt like I was wasting my time with the package,
and I decided to go for upstream install.
It turns out that installing Gitolite from upstream is damn easy and well-documented,
so that's what we're going to do in this post.



## Prerequisites

I assume we have to machines: a server and a workstation.
We're going to install and configure Gitolite on the server.
Of course, there's a SSH server up and running on the server.
The server hostname is `server`, our username is `bob`.
We use Debian (though it doesn't matter much).



## Installing (server-side)

At first, let's ensure that every dependencies for Gitolite are satisfied.
For that, `apt-cache` can help, even if we're not going to install Gitolite with apt.

	apt-cache show gitolite3 | grep ^Depends
	apt-get install git git-core perl

We need a new user that will host our git repositories.
I name it `git`, but if you don't like that just pick another name.

	adduser git

Let's switch to this new user, then get Gitolite sources from upstream:

	su - git
	git clone git://github.com/sitaramc/gitolite

I prefer to run an official version rather than the current development version.
A simple `git checkout` of the latest tag will do.

	cd gitolite
	git tag -l
	git checkout v3.6.3

Now, let's install Gitolite. There are a few options for installation,
you can check it out with `./install -h`.

	cd
	mkdir bin
	gitolite/install -ln
	ls -l bin
	  total 0
	  lrwxrwxrwx 1 git git 31 Sep 25 15:22 gitolite -> /home/git/gitolite/src/gitolite



## Setting up

At this point, Gitolite is installed and almost ready to go.
We just need to feed it the administrator's public key.
So, back on your workstation, create a SSH keypair if you don't have one:

	ssh-keygen

Then copy the public key to the server.
The file name is important here, it should be your name as a Gitolite user.

	scp ~/.ssh/id_rsa.pub git@server:~/admin.pub

Back on the server, you can now finish to configure Gitolite:

	gitolite setup -pk admin.pub
	rm admin.pub

Done!



## Testing

Now you can try the most basic test on your workstation:

	$ ssh git@server
	PTY allocation request failed on channel 0
	hello admin, this is git@server running gitolite3 v3.6.3-0-g5d24ae6 on git 2.1.4

	  R W	gitolite-admin
	  R W	testing
	Connection to server closed.

The fist line talking about 'request failed' is OK. This is due to the way Gitolite uses SSH.
To unveil this black magic, there's plenty of explanation on the official Gitolite website.
You may also have a look at the `~/.ssh/authorized_keys` file for the git user (server-side).



## Administrating

Now that everything is setup, you can start administrating your repositories,
using the special repo `gitolite-admin`. You do that from your workstation.

	$ git clone git@server:gitolite-admin

Let's suppose you want to rename the user `admin` into `bob`:

	$ cd gitolite-admin
	$ git mv keydir/admin.pub keydir/bob.pub
	$ sed -i 's/admin/bob/' conf/gitolite.conf
	$ git commit -am "Rename user: admin -> bob"
	$ git push

That was easy, wasn't it?



## Upgrading

Upgrading to a newer version of Gitolite is done in 3 steps:

 - Enter the Gitolite sources and run `git pull` to get the latest version
 - Then use the same install command as the one that was used to install (with option `-ln` here)
 - Then run `gitolite setup`

