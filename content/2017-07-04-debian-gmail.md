Title: Using GMail to send system mails on Debian
Category: sysadmin
Tags: debian, exim, gmail, mail, msmtp, planet-debian

If you're a Debian user like me, chances are that, at some point, you will
need to send emails from the command-line. It could be on a Debian server,
where you want to send notifications (login, logout, boot, daily report, ...).
Or it could be on your own machine, where you use some tools like `reportbug`
or `git send-email`.
<!-- PELICAN_END_SUMMARY -->

On this page we will go through the configuration needed to make it happen.
I assume that we will send the mails through [GMail](https://mail.google.com).
Then we'll go through the config steps of [Exim4](http://www.exim.org/), which
is the MTA (Mail Transfer Agent) usually installed on Debian.

At last, we will look at the config steps for [msmtp](http://msmtp.sourceforge.net/),
a SMTP client, which can be a lightweight alternative to Exim, if for some
reasons you want an alternative.

**UPDATE**: This tutorial is outdated, please refer to the new tutorial:
<https://arnaudr.io/2020/08/24/send-emails-from-your-terminal-with-msmtp/>



GMail setup
-----------

On the GMail-side, there's a little bit to do.

First, you want to ensure that the *2-Step Verification* is enabled. For that
to happen, visit <https://myaccount.google.com/security> and enable the 2-Step
Verification feature.

This will require that you authorize all your devices once (mail client, phone
and so on), which can be a bit tedious, but it's only for once. And after it's
done, you're left with a more secured account, so it's not that bad, right?

Enabling the 2-Step Verification will unlock the feature we want: *App Passwords*.
Visit <https://myaccount.google.com/apppasswords>, and go generate a password
for your application. You should use it only for *one* application, and never use
it anywhere else. The idea is that for each application that will send emails
through your GMail account, you create a password. Simple, right?

There are real benefits with app passwords:

- you won't have to write down your real password in a configuration file.
- gmail won't block your emails for security reasons (happens if you
  send emails from different servers in different locations, for example).
- you can change your GMail password without impacting applications.
- you can revoke an app password anytime without impacting anything else.

OK, once you've done the GMail setup, keep going.



Exim
----

If you're on a Debian system, it's likely that `exim` is already installed.
You can easily check that with `dpkg -l | grep exim`. If it's not there, then
install!

	apt-get install exim4 mailutils

Then, all you need is a little conf.

	dpkg-reconfigure exim4-config

And here's the answers I used (most of it is the default):

- General type of mail configuration: `mail sent by smarthost; no local mail`
- System mail name: `localhost`
- IP-addresses to listen on for incoming SMTP connections: `127.0.0.1 ; ::1`
- Other destinations for which mail is accepted: *leave blank*
- Visible domain name for local users: `localhost`  
- **IP address or host name of the outgoing smarthost: `smtp.gmail.com::587`**
- Keep number of DNS-queries minimal (Dial-on-Demand)? `No`
- Split configuration into small files? `No`

Now, it's time to store your password in plain text. This is where you're happy
to use an app password instead of your account password.

In case you're a bit lost, I'm talking here about an app password that you
have to generate from your Google account page, mentioned above.

	echo '*.google.com:YourAddress@gmail.com:YourAppPassword' \
	>> /etc/exim4/passwd.client

Then ensure the file has the right ownership and permissions:

	chown root:Debian-exim /etc/exim4/passwd.client
	chmod 640 /etc/exim4/passwd.client

At last, restart exim:

	update-exim4.conf
	systemctl restart exim4

If you made it up to here, it's time for a reward! Send yourself a little email
to see if everything is OK.

	echo 'Time for coffee buddy' | mail -s "Coffee break" AnyAddress@Wherever.com

#### Aliases

Aliases are used to redirect mails for local recipients. For example, if you wish
to associate an email address with `root`, add this kind of line to the file
`/etc/aliases`.

	root: RootAddress@Wherever.com

Now, sending an email to root is easier.

	echo 'Root are you there?' | mail -s "Root message" root

#### Tips and Tricks

On the GMail App Passwords page, you can see the last time a password was used.
If you don't see this date being bumped while you send an email, you know that
your mail didn't even reach this step.

You can also look at the "Send Mails" section of your GMail mailbox, your mails
should leave a trace there.

If the mail is send but doesn't seem to arrive, always check your Spam.

#### References

- `man exim4-config_files`
- [Debian Wiki - Gmail and Exim4](https://wiki.debian.org/GmailAndExim4)
- [Setting up Exim4 with GMail and 2-factor Authentication](https://www.talk-about-it.ca/setting-up-exim4-with-gmail-and-2-factor-authentication)



MSMTP
-----

Msmtp is lightweigth, very simple to install and configure. If you just want
to be able to send emails, it's a great alternative to Exim. Here's the way
to go.

Install is a one-liner as usual.

	apt-get install msmtp

Create the configuration for the current user.

```
cat << EOF > ~/.msmtprc
# Set default values for all following accounts.
defaults
auth		on
tls		on
tls_trust_file	/etc/ssl/certs/ca-certificates.crt
logfile		~/.msmtp.log

# A gmail account
account		gmail
host		smtp.gmail.com
port		587
from		YourAddress@gmail.com
user		YourAddress
password	YourAppPassword

# Set a default account
account default : gmail
EOF
```

Explicitly set the right permissions on the configuration file.

	chmod 600 ~/.msmtprc

Now is the time to test!

	cat << EOF | msmtp -t -a default
	To: Recipient@Wherever.com
	From: YourAddress@gmail.com
	Subject: Cafe Sua Da
	
	Iced-coffee with condensed milk
	EOF

#### MTA

Maybe you don't want to explicitly use msmtp to send emails. Maybe you have
some applications that expect a regular MTA program to be installed, and
will try to use the standard command `sendmail` to send emails.

Good news, it's just a matter of symlinking msmtp, and there is a package
that does just that for you.

	apt-get install msmstp-mta

Now you can test one last time.

	cat << EOF | sendmail Recipient@Wherever.com 
	Subject: Flat White
	
	The milky way of making coffee
	EOF

#### References

- <https://wiki.archlinux.org/index.php/Msmtp>

