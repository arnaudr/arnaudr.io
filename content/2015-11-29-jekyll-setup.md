Title: Getting Started With Jekyll
Category: blogging
Tags: jekyll

Here's a brief guide to install [Jekyll](http://jekyllrb.com/)
using the [Gem](https://rubygems.org/) package manager,
and some basic configuration.
<!-- PELICAN_END_SUMMARY -->



## Introduction

Before anything, I highly recommend that you take some time and read the
official Jekyll documentation: <http://jekyllrb.com/docs/home/>.
It's damn well written, trust me! It's so good that I must admit I stole pretty much everything
from them. Yes... This guide is mostly a quick summary of the official doc.
I go even further and quote their "types & hints" word for word.
So why do you keep on reading this post?

Probably because you're like me, you'd like to be finished before you're started. The less you read, the better.
Well it's up to you, but it won't be long until you HAVE TO go through the doc anyway, remember my word.
Jekyll is nice and all, but it's a blend of different technologies, and it's not easy to get the whole picture.
It's quick to get started, but as soon as you start customizing the thing, it's not easy anymore.
And if the web technologies are not the kind of water in which you usualy swim (like me), it's gonna be painful...

Alright, one last word before we start, why Jekyll? Why not Octopress or Ghost?

[Octopress](http://octopress.org) is "*an obsessively designed toolkit for Jekyll blogging*" (just quoting them).
My definition would be *a Jekyll helper*. If you want to go with it, here's my warnings:

- You will have to deal with Jekyll anyway, Octopress doesn't hide it all over. And by dealing with Jekyll,
  you deal with the various languages and technologies it uses, such as
  [html](https://en.wikipedia.org/wiki/HTML),
  [css](https://en.wikipedia.org/wiki/Cascading_Style_Sheets),
  [sass](http://sass-lang.com/),
  [yaml](http://yaml.org/),
  [liquid](http://liquidmarkup.org)...
  It's already complicated, you see. By using Octopress, you add one more piece to the puzzle.
- Also, cross your fingers that Octopress is actively maintained, because Jekyll IS very active and moving fast.
- BTW, the current Octopress version is 3. Try to remember that, OK? Most of the doc you will find on the web refers
  to the version 2, it will only lead you astray...

When I started this blog, I started with Octopress. But the more I used it, the more I felt it wasn't helping anything.
So in the end I dropped it, and started again from scratch, pure Jekyll this time. But maybe Octopress is not for me,
that's all. If you want to go with Octopress, here's the best tuto I found:
[Setting up a free blog](http://www.610yesnolovely.org/2014/12/31/setting-up-a-free-blog.html).

So what about [Ghost](https://ghost.org/) now? Ghost is a blogging platform written in Javascript.
It has NOTHING TO DO with Jekyll, it's completely different from a technical point-of-view.
The only common point I can see between both is that you will write your blog using the
[Markdown](http://daringfireball.net/projects/markdown/) language (but other markup languages are available though).
Ghost is good if you want to write your blog the [WordPress](https://wordpress.org/) way (that is, using a web browser).

Jekyll is good for any of the following reasons:

- if you want write your blog using your usual text editor (I'm writing with [Vim](http://www.vim.org/) at the moment).
- if you want your blog to be very lightweight, clean and simple.
- if you don't want your content to be stored in a database. Instead, each post is a markdown file.
- if you like to make your hands dirty ;)



## Jekyll installation

Alright, after all this talking, let's come to the point. Installing Jekyll is straight-forward.
If you want to have the latest version, you should not install it through your package manager, but use Gem instead.
Jekyll is written in [Ruby](https://www.ruby-lang.org/), and [Gem](https://rubygems.org/) is the Ruby Package Manager.

	apt-get install ruby
	gem install jekyll

That's all :)

In a near future, when you will want to update Jekyll, you will type something like that.

	gem update

And when things will break for no apparent reason, you will cross your fingers and invoke the following spell.

	gem cleanup



## Creating a new blog

This is again very easy.

	jekyll new blog
	cd blog
	jekyll serve

Now switch to your web browser. Jekyll comes with a built-in development server, perfect for preview.
By defaut, it serves on the local port 4000, so just enter this address in your web browser, and witness the miracle:

	http://localhost:4000

That's it, your blog is already ready! What's cool is that the server watches most of the input files,
so when you edit a post, you can just hit F5 in your web browser to see the changes.

By default, Jekyll only serves the localhost. So if you're running it on a remote machine, you need to tell it
to serve everyone with the `--host` option. The command looks like that:

	jekyll serve --host=0.0.0.0




## Playing with Dr. Jekyll

Here are some of the things that you may try to get acquainted with Jekyll:

- Edit the file `_config.yml`. First warning here (look carefully at the comment at the beginning of the file):

  > For technical reasons, this file is *NOT* reloaded automatically when you use
  > 'jekyll serve'. If you change this file, please restart the server process.

  You've been warned. Want another warning about this file?

  > Do not use tabs in configuration files
  > This will either lead to parsing errors, or Jekyll will revert to the default settings. Use spaces instead.

  Why's that? Well, you see the extension of the file, `.yml`? It means that the file is written with the Yaml language.
  And tabs are not allowed in Yaml.

- Write your first post. It happens in the `_posts` directory. But wait, beware of the filename!

  > How you name files in this folder is important.
  > Jekyll requires blog post files to be named according to the following format:
  >
  > YEAR-MONTH-DAY-title.MARKUP

  If you don't do that, the file is simply ignored by Jekyll. Also, don't forget to start your
  post with a Yaml Front Matter. Have a look at the example post provided by Jekyll and you should be fine.

- Add some defaults in `_config.yml`. For example, if you're the only writter of your blog, and you don't want
  to fill the `author` field at the beginning of each post, you can add a default value for that. It looks like that:

  	# Defaults settings
  	defaults:
  	  -
  	    scope:
  	      path: ""
  	    values:
  	      author: "Your name"



## When Jekyll becomes Mr. Hyde

It's been so easy that you certainly want to go further, start to customize the css, install a theme, maybe install a
plugin, who knows... Well I won't go any further with you. It won't be easy anymore. I mean, for people like me who
are not dealing with the web development daily. But hopefully you know better than me, and you will enjoy the power
and flexibility that Jekyll provides... Good luck!

