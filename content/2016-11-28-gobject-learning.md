Title: Learning GObject
Category: dev
Tags: c, glib, gobject, gnome

I worked on a C/GObject project for a while now, and had to learn GObject from
scratch. Learning GObject is definitely not the easiest thing to do, it's much
more than just learning to use a new library. Hence this "bookmark article",
that list the best links I found so far. Hope this helps other learners like me.
<!-- PELICAN_END_SUMMARY -->



How Do I Learn GObject?
-----------------------

First, you need to bookmark the official documentation. It's the best place to
start, but also a good place to go back afterward. Things that don't make sense
at the first reading will become clear as you go further.

- [GObject Reference Manual](https://developer.gnome.org/gobject/stable/)

Second, there's one essential article that nicely sums up the GObject
construction process, and raise some important points around it. This article
you will have to read again and again.

- [A gentle introduction to gobject construction](https://blogs.gnome.org/desrt/2012/02/26/a-gentle-introduction-to-gobject-construction/)

At last, you need code example, although the code you find is not always
up-to-date with latest/best way to do things. Anyway. The main GObject-based
libraries are probably the best places to go to learn GObject. Grab the source
code, sharpen your grep, and there you go.

- [GTK+](https://developer.gnome.org/gtk3/stable/)
- [GStreamer](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/)



Good Practices
--------------

### Follow The Conventions

The first place to learn about good practices is the GObject conventions page.
It's more than good practice actually. GObject expects you to follow a few
conventions, so that code generation tools (`glib-mkenum`) and other stuff can
actually work. Don't try to avoid that.

- [GObject Conventions](https://developer.gnome.org/gobject/stable/gtype-conventions.html)

### Use `G_DECLARE_*` And `G_DEFINE_*`

Do not clutter the header file with too much boilerplate, as it's often seen in
some old GObject-based code.

- [G_DECLARE_FINAL_DERIVABLE_TYPE](https://blogs.gnome.org/desrt/2015/01/27/g_declare_finalderivable_type/)

### Do The Minimum In `_init()`

There should be almost nothing in the `init()` function. Most of the time,
it just boils down to setting up the private pointer, if any.

For all the rest, it's better to implement it in the `constructed()` method.

- [A gentle introduction to gobject construction](https://blogs.gnome.org/desrt/2012/02/26/a-gentle-introduction-to-gobject-construction/)

### Always Use A Private Pointer

Using a private structure is particularly useful for derivable objects, since
for these objects, the structure MUST BE defined in the header (public) to allow
inheritance. But when the object is not derivable, and therefore the structure
is defined in the code (private), what the point of having a private struct?
Answer: consistency, and less hassle when the objects change from final to
derivable, or the other way around.

- [Changing quickly between a final and derivable GObject class](https://blogs.gnome.org/swilmet/2015/10/10/changing-quickly-between-a-final-and-derivable-gobject-class/)

### Best Way To Set Properties

The correct way to set an object pointer. It's not as trivial as it seems.

- [Use g_set_object() to simplify (and safetyify) GObject property setters](https://tecnocode.co.uk/2014/12/19/use-g_set_object-to-simplify-and-safetyify-gobject-property-setters/)



Quick FAQ
---------

### How To Change A Construct-Only Property Via Inheritance

This questions is asked on StackOverflow:

<http://stackoverflow.com/q/16557905/776208>

Unfortunately, the answer given by Emmanuele Bassi is wrong, and it seems that
the only other solution is to hack around, as did the guy who asked the question.

There's another solution though: don't use construct-only properties if they are
to be modified by the inherited objects. Make them constuct.

### How To Implement A Singleton

This is discussed here and there on the Net.

- [GObject API Reference](https://developer.gnome.org/gobject/unstable/gobject-The-Base-Object-Type.html#GObjectClass)
- [How to make a GObject singleton](https://blogs.gnome.org/xclaesse/2010/02/11/how-to-make-a-gobject-singleton/)

### How To Have Protected Fields

GObject doesn't provide any solution to have protected fields. I managed to do
without up to now, but just in case, here's a workaround. Basically, it's just
about defining the private structure in the header (therefore making it public),
and naming it 'protected' instead of 'private'.

- [GObject and protected fields – simple hack](http://codica.pl/2008/12/21/gobject-and-protected-fields-simple-hack/)

