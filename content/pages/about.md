Title: About

I'm Arnaud Rebillout, a French software engineer living in Ho Chi Minh City,
Vietnam.

I work in the field of Free and Open-Source Software (usually shortened as
[FOSS][]) and [Linux][] operating systems. It means things like [embedded
systems][], [IoT][] and [DevOps][].

I studied computer science at the University of Bordeaux 1, France, where I
obtained my master's degree with honors in 2007. Since then, I lived and worked
in France, Australia, Switzerland and Vietnam.

I currently work as a freelance consultant.

[devops]: https://en.wikipedia.org/wiki/DevOps
[embedded systems]: https://en.wikipedia.org/wiki/Embedded_system
[foss]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[iot]: https://en.wikipedia.org/wiki/Internet_of_things
[linux]: https://en.wikipedia.org/wiki/Linux



<h2 class="line-on-sides"><span>Skills</span></h2>



### Linux operating systems

I'm a [Debian Developer][] and I can support your business in using Debian for
various projects, or maintain your existing Debian infrastructure, or anything
in between.

[Debian][] is a general-purpose Linux operating system, widely used server-side
(either in containers, or as a host OS), and also suitable for IoT devices and
embedded systems. Debian is the OS on top of which is built [Ubuntu][].
 
I can also deliver projects with [Buildroot][], a tool that specializes in
building highly tailored Linux OS for embedded devices. Buildroot is best for
low-resource devices.

| Keywords      | |
| ------------- | -------------------------------- |
| Distributions | Debian, Ubuntu, Arch Linux |
| Embedded      | Buildroot, OpenWRT, cross-compilation |

[debian developer]: https://qa.debian.org/developer.php?login=arnaudr%40debian.org
[buildroot]: https://buildroot.org/
[debian]: https://www.debian.org/
[ubuntu]: https://ubuntu.com/

----

### DevOps

DevOps can be loosely defined as a set of tools and practices that help your
business deliver software, with focus on continuous delivery and quality.

I can help your business to understand what DevOps is, and how it can help you.
We can analyze your software development and delivery process, understand where
are the pain points, and see how to improve it with DevOps methodology.

DevOps covers various topics such as infrastructure automation (or
[infrastructure as code][]), [continuous integration][], [automatic testing][],
[packaging][], [release automation][].

| Keywords | |
| ------------------- | -------------------------------- |
| Build & Integration | Azure Pipelines, GitLab CI/CD, Jenkins, Open Build Service |
| Containers & VM     | Docker, Vagrant, libvirt, QEMU |
| Config Management   | Ansible, SaltStack |

[infrastructure as code]: https://en.wikipedia.org/wiki/Infrastructure_as_code
[continuous integration]: https://en.wikipedia.org/wiki/Continuous_integration
[automatic testing]: https://en.wikipedia.org/wiki/Continuous_testing
[packaging]: https://en.wikipedia.org/wiki/Package_manager
[release automation]: https://en.wikipedia.org/wiki/Application-release_automation

----

### Software development and Open-Source

I can write code for your business, either applications or libraries, as long
as it's expected to run on a Linux OS. I write mainly in *Python*, *C* and
*Bash*, although I have experience with many other languages such as
*Javascript*, *HTML*, *CSS*, *Go* and more.

One aspect of writing code for an open OS such as Linux is that often, you
don't need to write code, you can instead reuse code that already exists, if
you can find it.  Another aspect is that, if you really need to write code,
this code will rarely sit alone, instead it needs to interact with the rest of
the OS, and you'll need a good knowledge of Linux to get things right.

We'll work together to understand exactly what problem you need to solve, and
you'll benefit from my 15+ years of experience in Linux & Open-Source software.

| Keywords   | |
| ---------- | -------------------------------- |
| Languages  | Bash, C, Python, Go, Javascript |
| Build      | Makefile, Meson, Autotools |
| Deployment | Debian packaging, Docker images, Flatpak |
| Libraries  | GNOME, GTK, GStreamer, GLib, GObject |

----

### Web development

While not my specialty, I always enjoy a bit of web development.  I can handle
the creation and deployment of the website for your product, and I can develop
simple web services if needed.

| Keywords     | |
| ------------ | -------------------------- |
| Web Servers  | Apache, NGINX, Flask |
| Languages    | HTML, CSS, Markdown, Jinja |
| Static Sites | Hugo, Jekyll, Pelican |



<h2 class="line-on-sides"><span>Experience</span></h2>

<div class="experience">
  <div class="experience-half1">
    <table>
      <tbody>
        <tr>
          <td><i class="fa fa-calendar"></i></td>
          <td>2021 → now</td>
        </tr>
        <tr>
          <td><i class="fa fa-map-marker"></i></td>
          <td>Remote</td>
        </tr>
        <tr>
          <td><i class="fa fa-gears"></i></td>
          <td>Software Engineer</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="experience-half2">
    <a href="https://www.kali.org" target="_blank">
      <picture>
        <source srcset="/images/kali.svg" media="(min-width: 48em)">
        <img src="/images/kali-logo-dragon-blue.svg" alt="Kali Linux"/>
      </picture>
    </a>
  </div>
</div>

[Kali Linux][] is the leading Linux distribution for infosec work. It's used
for pentesting, security research, computer forensics, and more.

Kali is a rolling distro based on Debian. I work on packages, develop tools,
and maintain the infrastructure that makes it happen everyday.

[kali linux]: https://www.kali.org/

----

<div class="experience">
  <div class="experience-half1">
    <table>
      <tbody>
        <tr>
          <td><i class="fa fa-calendar"></i></td>
          <td>2018 → 2020</td>
        </tr>
        <tr>
          <td><i class="fa fa-map-marker"></i></td>
          <td>Remote</td>
        </tr>
        <tr>
          <td><i class="fa fa-gears"></i></td>
          <td>Software Engineer</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="experience-half2">
    <a href="https://www.collabora.com" target="_blank">
      <picture>
        <source srcset="/images/collabora.svg" media="(min-width: 48em)">
        <img src="/images/collabora-stacked.svg" alt="Collabora"/>
      </picture>
    </a>
  </div>
</div>

[Collabora][] is an international consultancy that specializes in Open-Source
software.

I was in charge of building and maintaining a Debian derivative aimed for the
desktop.  A large part of the job is about improving processes, automating
everything and working on test frameworks, so basically QA and DevOps things.
Another part is about making the OS read-only and working on the update
mechanism.

[collabora]: https://www.collabora.com/

----

<div class="experience">
  <div class="experience-half1">
    <table>
      <tbody>
        <tr>
          <td><i class="fa fa-calendar"></i></td>
          <td>2017</td>
        </tr>
        <tr>
          <td><i class="fa fa-map-marker"></i></td>
          <td>Remote</td>
        </tr>
        <tr>
          <td><i class="fa fa-gears"></i></td>
          <td>Lead Developer</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="experience-half2">
    <a href="https://www.kickstarter.com/projects/preevio/silentkeys-a-keyboard-that-protects-your-privacy-a" target="_blank">
      <picture>
        <source srcset="/images/preevio.svg" media="(min-width: 48em)">
        <img src="/images/preevio-stacked.svg" alt="Preevio"/>
      </picture>
    </a>
  </div>
</div>

[Preevio][] was a French start-up developing privacy & security oriented
products.

I worked on Satya, a derivative of the portable operating system [Tails][]. The
job was about building a Debian derivative, writing GTK applications and
customizing the GNOME desktop with various extensions.

[preevio]: https://www.kickstarter.com/projects/preevio/silentkeys-a-keyboard-that-protects-your-privacy-a
[tails]: https://tails.net/

----

<div class="experience">
  <div class="experience-half1">
    <table>
      <tbody>
        <tr>
          <td><i class="fa fa-calendar"></i></td>
          <td>2011 → 2014</td>
        </tr>
        <tr>
          <td><i class="fa fa-map-marker"></i></td>
          <td>Switzerland</td>
        </tr>
        <tr>
          <td><i class="fa fa-gears"></i></td>
          <td>Lead Developer</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="experience-half2">
    <a href="http://www.syscom.ch" target="_blank">
      <picture>
        <source srcset="/images/bartec-syscom.svg" media="(min-width: 48em)">
        <img src="/images/bartec-syscom-stacked.svg" alt="Bartec Syscom"/>
      </picture>
    </a>
  </div>
</div>

[Bartec Syscom][] is a Swiss company that produces vibration and seismic
monitoring equipment.

I developed the embedded Linux system for a brand new line of equipment,
starting from scratch. The OS is cross-compiled for Arm with Buildroot.

[bartec syscom]: http://www.syscom.ch

----

<div class="experience">
  <div class="experience-half1">
    <table>
      <tbody>
        <tr>
          <td><i class="fa fa-calendar"></i></td>
          <td>2007 → 2009</td>
        </tr>
        <tr>
          <td><i class="fa fa-map-marker"></i></td>
          <td>France</td>
        </tr>
        <tr>
          <td><i class="fa fa-gears"></i></td>
          <td>Software Engineer</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="experience-half2">
    <a href="https://www.sfr.fr" target="_blank">
      <img src="/images/sfr.svg" alt="SFR"/>
    </a>
  </div>
</div>

[SFR][] is a French telecommunications company.

I was part of the team in charge of developing the firmware for various
consumer routers.  In other words, highly customized embedded Linux OS for
low-resource devices, built with OpenWRT.

[sfr]: https://www.sfr.fr



<h2 class="line-on-sides"><span>Side-projects</span></h2>



### Goodvibes

Since 2016, I develop and maintain [Goodvibes][gv-gitlab], an Internet radio
player for Linux.

It is complete with unit tests, [continuous integration][gv-gitlabci], [user
documentation][gv-rtd], and it is [translated][gv-weblate] in around 10
languages. The app is distributed via the [Flathub app store][gv-flathub], and
packaged for [various Linux distributions][gv-repology].

Apart from the fun, maintaining this little program is a good way to stay in
touch with a number of technologies related to GUI programming, multimedia, and
Linux desktop environments. It's also a chance to handle every parts of an
application lifecycle, from creation to delivery.

[gv-gitlab]: https://gitlab.com/goodvibes/goodvibes
[gv-gitlabci]: https://gitlab.com/goodvibes/goodvibes/-/pipelines
[gv-rtd]: https://goodvibes.readthedocs.io/en/stable
[gv-weblate]: https://hosted.weblate.org/projects/goodvibes/translations
[gv-flathub]: https://flathub.org/apps/details/io.gitlab.Goodvibes
[gv-repology]: https://repology.org/project/goodvibes/versions

----

### This blog

I started this blog in 2015, and it's a little project in itself.

This blog is created thanks to the static site generator [Pelican][], and
building it required some basic knowledge of *HTML*, *CSS*, the *Jinja*
templating language, and a bit of *Python*.

The everyday operation is about typing some *Markdown* text, and sending it to
the right places thanks to *Rsync*, *Git* and *SSH*.

It's hosted on a *Debian* server that is managed with *Ansible*, and served
thanks to the web server *Apache*. 

[pelican]: https://blog.getpelican.com/

----

### Guitar Looper with an Arduino board

Back in 2009, I made a guitar looper with an [Arduino][] board, a bunch of
pedals, and the [Pure Data][] programming language. The original [blog
post][looper-blog-post] is still around to testify!

[arduino]: https://www.arduino.cc/
[pure data]: https://puredata.info/
[looper-blog-post]: http://arduino-guitarlooper.blogspot.com/2009/10/introduction-here-how-to-produce-small.html



<h2 class="line-on-sides"><span>Contact</span></h2>



I live in Ho Chi Minh City, Vietnam. If you're around or if you're not afraid
of remote work, [get in touch](mailto:arnaudr@debian.org)!



| | | |
| ------------------------------ | ----- | --------------------------- |
| <i class="fa fa-envelope"></i> | Email | <mailto:arnaudr@debian.org> |
| <i class="fa fa-gnupg"></i>    | PGP | [9D3B EC87 B456 3C64 2B7B  E38A C083 1D1F 15E0 DA64](http://keyserver.ubuntu.com/pks/lookup?op=vindex&search=0x9D3BEC87B4563C642B7BE38AC0831D1F15E0DA64&fingerprint=on) |
| <i class="fa fa-linkedin"></i>       | LinkedIn      | <https://www.linkedin.com/in/arnaud-rebillout> |
| <i class="fa fa-stack-overflow"></i> | StackOverflow | <https://stackoverflow.com/users/story/776208> |
| <i class="fa fa-github"></i> | GitHub | <https://github.com/elboulangero> | 
| <i class="fa fa-gitlab"></i> | GitLab | <https://gitlab.com/arnaudr> |
| <i class="fa fa-debian"></i> | Debian | <https://salsa.debian.org/arnaudr> |
| | GNOME  | <https://gitlab.gnome.org/arnaudr> |
| | Freedesktop | <https://gitlab.freedesktop.org/arnaudr> |
