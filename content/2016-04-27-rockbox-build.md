Title: Building And Installing Rockbox
Category: dev
Tags: embedded, rockbox

Here is a very quick howto that walks you through the few steps
needed to compile the [Rockbox](http://www.rockbox.org) firmware from source.
<!-- PELICAN_END_SUMMARY -->


## Get yourself ready

Just make sure that you have all the basic tools installed.

	apt-get install build-essential libtool-bin flex bison texinfo



## Get the source code

Rockbox is versioned on github:

	git clone git://git.rockbox.org/rockbox



## Build the toolchain

This is documented on the official wiki: <http://www.rockbox.org/wiki/CrossCompiler>.

	cd rockbox
	export RBDEV_PREFIX="/opt/rockbox"
	sudo mkdir "$RBDEV_PREFIX"
	# Set yourself as the owner of the build directory to avoid compiling as root
	sudo chown $USER:$USER "$RBDEV_PREFIX"
	cd tools
	./rockboxdev.sh
	# Set ownership back to root
	sudo chown -R root:root "$RBDEV_PREFIX"

It may happen that the build fails with such error:

	../../gcc-4.4.4/gcc/toplev.c: At top level:
	../../gcc-4.4.4/gcc/toplev.c:536:1: error: redefinition of ‘floor_log2’
	floor_log2 (unsigned HOST_WIDE_INT x)

In such case, you need to modify the CFLAGS used to build the toolchain, as suggested on
this page: <http://www.rockbox.org/irc/logs/rockbox-20150803.txt>. At the moment it's
at the line 214 of the file `rockboxdev.sh`. Replace `CFLAGS=-U_FORTIFY_SOURCE` by
`CFLAGS="-U_FORTIFY_SOURCE -fgnu89-inline"`.



## Build the firmware

This is documented on the official wiki: <http://www.rockbox.org/wiki/HowToCompile>

	mkdir build && cd build
	PATH=/opt/rockbox/bin:$PATH
	../tools/configure
	make -j

Then, you need to pack all the firmware files in an archive. For the small archive,
use the target `zip`. For a bigger archive with additional stuff (like plenty of fonts),
use the target `fullzip`.

	make fullzip



## Install the firmware on your device

I assume you already installed Rockbox before on your favorite audio player,
and you're just updating the firmware right now. It means that I won't explain
how to install the bootloader.

So just plug your device, mount it. Here we assume it's mounted at the location
`/media/$USER/iPodClassic` (yep I've got an iPod, now you know).

Installing the firmware is a simple as unpacking the archive at the root your
device filesystem. The archive only contains a directory named `.rockbox` that
holds the firmware files. If you really want to restart from scratch, you can
delete this directory beforehand.

	rm -fr /media/$USER/iPodClassic/.rockbox

Otherwise, just unpack the archive and overwrite everything.

	unzip rockbox-full.zip -d /media/$USER/iPodClassic/
	  Archive:  rockbox-full.zip
	  replace /media/user/iPodClassic/.rockbox/rockbox-info.txt? [y]es, [n]o, [A]ll, [N]one, [r]ename: A
	  inflating ...
	  ...
	sync

If unsure, backup before doing anything foolish!



## Reboot your audio player

That's it. Just reboot and unplug your audio player.

If you're the owner of an iPod Classic/6g like me, and tend to forget how to restart it,
here is the procedure: hold the "Menu" and "Select" buttons for about 5 seconds.

