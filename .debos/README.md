Pelican chroot
==============

Steps to setup the pelican chroot:

    PROJ=pelican-chroot

    debos -e http_proxy:http://10.0.2.2:3142 ${PROJ:?}.yaml

    OSPACK=${PROJ:?}.tar.gz
    CONF=${PROJ:?}.conf

    CHROOTDIR=$(sed -n 's/directory=//p' ${CONF:?})
    sudo mkdir ${CHROOTDIR:?}
    sudo tar -C ${CHROOTDIR:?} -xf ${OSPACK:?}
    rm ${OSPACK:?}

    CONFDIR=/etc/schroot/chroot.d
    CHROOT=$(basename ${CHROOTDIR:?})
    sudo cp ${CONF:?} ${CONFDIR:?}/${CHROOT:?}.conf
    rm ${CONF:?}

At this point, everything is ready:

    schroot -l

Update the chroot:

    CHROOT=buster-amd64-pelican
    schroot -c source:${CHROOT:?} -u root -- \
      bash -c "apt update && apt --yes dist-upgrade && apt clean"

Delete the chroot:

    CHROOT=buster-amd64-pelican
    sudo rm -fr /srv/chroot/${CHROOT:?}
    sudo rm /etc/schroot/chroot.d/${CHROOT:?}.conf
