Title: Backing Up Ghost Content
Category: blogging
Tags: backup, bash, ghost

My homemade script to backup my ghost blog, smartly named `ghost-backup`.
<!-- PELICAN_END_SUMMARY -->



## Introduction

Once you start investing time in things, backing it up is a must-have.
Blogging is one of these things that take time, and if you host your
blog by yourself, it's up to you to take care of the backup.



## References

Let's start with some good articles I read on the net here and there:

- <https://www.ghostforbeginners.com/backing-up-ghost-on-the-command-line/>
- <http://ahmed.amayem.com/different-methods-of-backing-up-and-automating-backups-of-ghost-on-linux-centos-6/>



## Important points

Your Ghost content lives in the `content` sub-directory. You can choose to backup:

- only the database at `content/data/ghost.db` (the text of your posts is here)
- the whole `content` directory (also includes pictures, theme, etc)

Ghost must be stopped before you copy the database. This is to ensure that there's
no transaction in progress in the database at the moment you read it.



## How I do it

I choose the most complicated path, as usual. I want to backup the whole content
because pictures are important too. But my blog lives on a VPS where I don't have
much space on the disk, so I can't afford to have too many backups, takes too much
space.

So I started to think. First, I can backup everyday, but for most of the content,
it's useless since it's not changed everyday. If you're curious and compare two
successive backups, let's says day 1 and day 2, the only thing that changed is
the database. Even if you don't edit your blog, the database is modified by Ghost.

The database doesn't take a lot of space, but is often modified. The pictures take
a lot of space, but are not modified often. So we have two different problems here,
and I thought I should solve them separately.

So my solution is to backup the whole `content` directory everyday. But then I
take the database out of it. I keep database backups for a long period, like 30 days.
The rest of the backup, I compare it against the previous backup. If the content is the
same, I just discard it. And I keep only few backups like this, because they're big.

In the end, I feel safe :) I've got a long period of database backup. And for the
pictures, I'm not even sure I really need to back it up, but we never know, so I've
got a few backups just in case.

If you're interested, you can check out my script on Gitlab:<br>
<https://gitlab.com/arnaudr/ghost-backup>
