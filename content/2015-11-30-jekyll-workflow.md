Title: My Daily Jekyll Workflow
Category: blogging
Tags: jekyll, git

My personal [Jekyll](http://jekyllrb.com/) workflow, involving
[Git](https://git-scm.com/), [Rsync](https://rsync.samba.org/),
and my own homebrewed script, the almighty Jekyllkenny...
<!-- PELICAN_END_SUMMARY -->



## Introduction

For people who already know about the "[GitHub Pages](https://pages.github.com/)"
workflow (you know, the weird two-branches workflow, one branch for the source,
the other for the compiled static site), I stop you right now. This is not what
I'm going to talk about.

My setup is slightly different, simpler and much more easier to picture.
I do not version the compiled static site. I use Git to version ONLY the source.
Then I use Rsync to update the compiled site on the server. That's all.
It's just the usual way of doing things, nothing fancy. And it's real easy to setup.



## Source versionning with git

So, just to tell you the context: I have a VPS where I version the source of my blog with git.
On the VPS, I manage my git repositories with [Gitolite](http://gitolite.com/)
(I have another post talking about that).

OK, so how do you configure that? It's damn easy if you're used to git.
First, as soon as you create your new blog, start versionning it.

	jekyll new blog
	cd blog
	git init
	git add .gitignore *
	git commit -m"Initial commit: jekyll skeleton"

Now, let's assume that your have a vps somewhere, which is known under the name `vps`.
I also assume that you access it through the user `git` (just the usual way), and that
you already created an empty git repository on the server, named `blog.git`. The following
commands come easy:

	git remote add origin git@vps:blog
	git push --set-upstream origin master

That's all, your blog is now versionned and safe, you can commit and push whenever you want,
and undo your worst mistakes (like the dreaded `rm -rf _posts`) without stress :)

Versionning with git is really the basic. Blogging is pretty much like coding, you can't afford
to loose it all once you started investing some time in it. So you must version, at least for
the sake of having a backup.



## Pushing to the server with rsync

This is only one of the several ways of sending your static blog to the server.
I have several reasons to choose rsync for this task:

- it's made for syncing data, you can't find a better tool for the task
- it uses SSH authentication with the server, something that's already setup
- I never heard nobody blame rsync :)
- it's easy to use

It just boils down to a simple command:

	rsync --archive --delete --compress \
	  --chown www-data:www-data \
	  ./_site/ user@vps:/var/www/blog

Notice some smart options here and there:

- `--chown` to change the user and group of the synced files
- a trailing slash at the end of the source `./_site/` to avoid creating a sub-directory inside `/var/www/blog`

Of course, it's tedious to remember such command each time. To make it easier, you can
alias it, or if you're brave enough, you can start writing a little script.



## Writing posts

I create a new post in the `_drafts` sub-directory. I work on it and preview locally,
using the `--draft` option of Jekyll.

	mkdir _drafts
	vi _drafts/my-post.md
	jekyll server --draft

When I want to publish, I twist the usual Jekyll workflow a little bit.
The common way would be just to move the draft to the post directory, and name it
according to the pattern expected by Jekyll, that's to say, prefixed by the date.
So the command would be:

	mv _drafts/my-post.md _posts/2015-11-18-my-post.md

This is no problem, except that I don't like too much this naming.
This is because my blog is mainly a memento for myself, where I write commands
and stuff that I would forget otherwise. When I need to recall something, I
don't look for it in my web browser. I'm a command-line guy, so I prefer to
go in my Jekyll post directory, and search in my posts directly.

That's one of the reasons I like Jekyll, because it doesn't bury my content in a
database, it just let the posts live in simples markdown files, that I can browse
anytime I feel like it. So when I list my posts with a simple `ls` command, I
would like them to be sorted according to their name (which make sense to me)
rather than their date.

In order to achieve that, I take advantage of the fact the Jekyll ignores the
file that don't have the proper naming. So I just name my files as I want,
them add a symlink with the proper name.

	mv _drafts/my-post.md _posts/my-post.md
	ln -s my-post.md _posts/2015-11-18-my-post.md

That's all, it's a simple trick, but I do appreciate that Jekyll let me do that.



## Jekyllkenny

In the end, I started to write my own little script to automate a few things, like
syncing or publishing. Basically, a kind of [Octopress](http://octopress.org/) but much
more simple, and taylored just for me.

If your Jekyll workflow meets mine, you can check it out on [Gitlab](https://gitlab.com)
and give it a try. Here is the address:

<https://gitlab.com/arnaudr/jekyllkenny>

