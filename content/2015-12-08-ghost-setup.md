Title: Getting Started With Ghost
Category: blogging
Tags: apache, ghost, nodejs, systemd

How to install Ghost from upstream, setup Apache as a frontend for Ghost,
and create a systemd service to start Ghost automatically.
<!-- PELICAN_END_SUMMARY -->



## Introduction

[Ghost](https://ghost.org/) is a blogging platform, a kind of
[Wordpress](https://wordpress.org/) that would have been re-written
from scratch, and that strives to remain simple.

In this post we will install and configure Ghost.

As often when we want to use the latest version, we're going to bypass
the package manager, and install the stuff from upstream. It's quite
easy with Ghost, no reason to be afraid ;)



## Node.js installation

Ghost is coded in [Node.js](https://nodejs.org/), it's the first
thing that we must install.

First, let's be sure that you have all the build tools needed.

	apt-get update
	apt-get install build-essential python

Now, get the right version of Node.js. Have a look at the
[Ghost installation page](http://support.ghost.org/installation/)
and check what's the recommanded version. At the moment it's the
`0.10.x` serie.

	VERSION=v0.10.41
	wget http://nodejs.org/dist/$VERSION/node-$VERSION.tar.gz
	tar -xvzf node-$VERSION.tar.gz
	cd node-$VERSION
	./configure
	make
	make install

Compilation takes some time...


## Ghost installation

You will need to unzip, be sure to be ready...

	apt-get install unzip

Now just go to the [Ghost download page](https://ghost.org/download/)
to see what is the latest version. Or you can also use the symlink
to the latest version: <https://ghost.org/zip/ghost-latest.zip>.
Unzip that where your websites usually go, it may be `/var/www`
or `/srv/www`.

	VERSION=0.7.1
	wget https://ghost.org/zip/ghost-$VERSION.zip
	unzip ghost-$VERSION.zip -d /srv/www/ghost-$VERSION
	ln -s ghost-$VERSION /srv/www/ghost

I like to symlink my current Ghost blog, so that I can have
different Ghost versions side to side, and switch from one to
another easily. But it's just my own way of doing things.



## Basic Ghost configuration

There is an example of configuration provided, so the best thing
to do is to copy it, and start from that point.

	cd /srv/www/ghost
	cp config.example.js config.js
	vi config.js

Actually, there's not much to change, you're already setup to get started.

You can see that there are two different configurations, one for
production and one for development. I only use the production, but
once again it's just my own use case, feel free to do better.

So, there's one thing you should change, it's the `url` value, which should
point to the URL of your blog.

	    // Configure your URL and mail settings here
	    production: {
	        url: 'http://your.blog.com',

Also, you should configure the mail settings right now,
that's what they advise in the official documentation.
I use [Mandrill](https://www.mandrill.com/) to send my emails,
here is the configuration:

	        mail: {
	            transport: 'SMTP',
	            host: 'smtp.mandrillapp.com',
	            options: {
	                service: 'Mandrill',
	                auth: {
	                    user: '<your-mandrill-username>',
	                    pass: '<your-mandrill-api-key>'
	                },
	            },
	        },

More details on this page: [Adding Mandrill to your ghost blog](http://rdbrdd.com/configuring-ghost-to-use-mandrill/)



## Testing

You can already test your setup. There's just one little thing to do.
Go to the config file. See the `server` part?

	        server: {
	            host: '127.0.0.1',
	            port: '2368'
	        }

The `host` setting tells Ghost to serve only the localhost. This is because Ghost
is not supposed to serve the outside world directly. There should be another
web server between Ghost and the outside wild world.

So, if you run Ghost on your machine, this is OK, you can test without having
anything to change. But if you run it on a remote server, just for this quick
test, you must change the `host` parameter, and set it to `0.0.0.0`. Just be
sure to set if back to `127.0.0.1` afterward, for security reasons.

Now, just run Ghost for the first time! I do that in production mode directly.
Be sure to run this commands in the Ghost directory, since `npm`
(the [Node Package Manager](https://www.npmjs.com)) installs plenty of stuff locally
inside the Ghost directory.

	cd /srv/www/ghost
	npm install --production
	npm start --production

That's it! Open your web browser, connect on the port `2368`, you should see
your blog. That was easy, wasn't it?



## Apache setup for Ghost

As we said, Ghost should run behind a web server, which acts as a frontend.

I personnally use [Apache](https://httpd.apache.org/), so I describe quickly
the Apache setup. If you want to use [Nginx](nginx.org), you'll be fine, there's
plenty of doc available on the web.

At first, be sure to have Apache installed.

	apt-get install apache2

Then, enable the HTTP proxy mode.

	a2enmod proxy_http

Create the Ghost site configuration. As you can see, all the magic comes
from the HTTP Proxy config.

	echo '
	<VirtualHost *:80>
	    ServerAdmin your@mail.com
	    ServerName your.blog.com
	    #ServerAlias your.blog.alias.com

	    ErrorLog ${APACHE_LOG_DIR}/ghost_vhosts_error.log

	   <Location />
	        ProxyPass http://localhost:2368/
	        ProxyPassReverse http://localhost:2368/
	   </Location>
	</VirtualHost>
	' > /etc/apache2/sites-available/ghost.conf

At last, enable the site:

	a2ensite ghost

Now, you can just restart Apache. I personnaly use Debian, and therefore it's
up to systemd to do the job.

	systemctl restart apache2

OK, so now, be sure that you have a node instance running
(remember the `npm start --production` command?).
You should be able to see your blog in a web browser.

You should understand that, when you request your blog, it's Apache who
gets the request first. Thanks to the `ServerName` setting
(and possibly `ServerAlias` if you have one), it redirects the request
to `localhost:2368`, where there's a node instance listening. This node
instance is the one that serves your blog for real.



## Getting Node to run by itself

Now, we're almost there. For the moment, you run the node instance manually
through the command `npm start`. That's OK for debug, but now we need it to
be running all the time, from the moment your server starts until it stops.

There are several ways to do that. I personnaly use
[systemd](http://www.freedesktop.org/wiki/Software/systemd/).

The first recommandation is to add a user that will run the node instance.
This is a good habit, to avoid running it as root. So, just create a new
user (we name it `ghost`, just to be simple), and let him own the Ghost directory.

	adduser --shell /bin/bash --gecos 'Ghost application' ghost
	chown -H -R ghost:ghost /srv/www/ghost

Now, here is the systemd unit file that is used to start/stop Ghost.
Notice how we ask to run as the user/group ghost. Notice also that
we set an environment variable. I think it's needed, but I'm not
100% sure though...

	echo '
	[Service]
	ExecStart=/usr/local/bin/node /srv/www/ghost/index.js
	WorkingDirectory=/srv/www/ghost
	Restart=always
	StandardOutput=syslog
	StandardError=syslog
	SyslogIdentifier=ghost
	User=ghost
	Group=ghost
	Environment=NODE_ENV=production
	
	[Install]
	WantedBy=multi-user.target
	' > /etc/systemd/system/ghost.service


Here you go, just start the service!

	systemctl enable ghost
	systemctl start ghost



## Start blogging

To start blogging, just open your web browser, visit the URL of your blog.
Append `/ghost` to the URL, this will lead you to the admin page.
Just create your account as you're asked. If you did setup your mail stuff
properly, you will receive an email once you finish creating your account.

And that's all about it, now your can start writing posts.

Ghost uses the [Markdown](https://daringfireball.net/projects/markdown/syntax) syntax,
so if you don't know about that yet, maybe it's your next step :)



## References

Here are some links that made me write this post:

- [Installer Ghost + Apache sur un VPS Debian 7 (Wheezy)](http://bloggle.me/installation-de-ghost-et-apache-sur-un-vps-debian)
- [A Ghost Workflow](http://seanvbaker.com/a-ghost-workflow/)
- [Adding Mandrill to your ghost blog](http://rdbrdd.com/configuring-ghost-to-use-mandrill/)
